package ute.udn.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Data
@Entity
@Table(name = "test_exam_question")
@NoArgsConstructor
@AllArgsConstructor
public class TestExamQuestion implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long questionId;
	
	private String audio;
	private String image;
	
	@Column(columnDefinition="TEXT")
	private String question;
	
	
	private String option1;
	private String option2;
	private String option3;
	private String option4;
	
	@Column(columnDefinition="TEXT")
	private String pragraph;
	
	private String corectOption;
	
	private String explains;
	
	private String chose;
	
	@ManyToOne
	@JoinColumn(name ="testExamId")
	private TestExam testExams;
	
	@OneToMany(mappedBy = "testExamQuestion", cascade = CascadeType.ALL)
	private List<HistoryExamTest>  historyExamTests;
}
