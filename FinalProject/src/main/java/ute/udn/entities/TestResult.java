package ute.udn.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "testResult")
public class TestResult implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long resultId;
	
	@Column(name = "time_start_listening_test")
	private Date timeStartListeningTest;
	
	@Column(name = "time_submit_listening_test")
	private Date timeSubmitListeningTest;
	
	private int totalListenCorrect;
	
	@Column(name = "time_start_reading_test")
	private Date timeStartReadingTest;
	
	@Column(name = "time_submit_reading_test")
	private Date timeSubmitReadingTest;
	
	private int totalReadingCorrect;

	
	private int totalCorrect;
	
	private Date totalTimeDoExam;
	
	
	@ManyToOne
	@JoinColumn(name ="id")
	private User users;
	
	@ManyToOne
	@JoinColumn(name ="testExamId")
	private TestExam testExam;
	
	@OneToMany(mappedBy = "testResult", cascade = CascadeType.ALL)
	private List<HistoryExamTest>  historyExamTests;
}
