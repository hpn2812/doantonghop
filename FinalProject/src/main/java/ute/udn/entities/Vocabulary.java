package ute.udn.entities;

import java.io.Serializable;
import java.util.List;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "vocabulary")
public class Vocabulary  implements Serializable{
	@Id
	@GeneratedValue(strategy =  GenerationType.IDENTITY)
	private Long vocabId;
	
	private String image;
	private String name;
	
	@OneToMany(mappedBy = "vocabulary", cascade = CascadeType.REMOVE)
	private List<ContentVocabulary>  contentVocabularies;
	
	@ManyToOne
	@JoinColumn(name = "id")
	private User users;

}
