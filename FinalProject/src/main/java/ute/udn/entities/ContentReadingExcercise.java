package ute.udn.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ContentReadingExcercise")
public class ContentReadingExcercise implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long crId;
	@Column(columnDefinition="TEXT")
	private String question;
	
	@Column(columnDefinition="TEXT")
	private String paragrahps;
	
	@Column(columnDefinition="TEXT")
	private String correctReading;
	private String a;
	private String b;
	private String c;
	private String d;
	private String explains;
	private String chose;
	
	
	@ManyToOne
	@JoinColumn(name = "readId" )
	private ReadingExcercise readingExcercise;

}
