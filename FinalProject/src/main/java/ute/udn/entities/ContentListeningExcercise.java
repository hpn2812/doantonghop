package ute.udn.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ContentListeningExcercise")
public class ContentListeningExcercise implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long cListentId;
	
	private String question;
	private String correctOption;
	
	@Column(nullable = false)
	private String option1;
	
	@Column(nullable = false)
	private String option2;
	
	@Column(nullable = false)
	private String option3;
	
	private String option4;
	@Column(columnDefinition="TEXT")
	
	private String explains;
	
	private String image;
	
	private String audio;
	
	private String chose;
	
	@Transient
	private byte[] photoData;
	
	@ManyToOne
	@JoinColumn(name = "listenId")
	private ListeningExcercise excercise;

	

}
