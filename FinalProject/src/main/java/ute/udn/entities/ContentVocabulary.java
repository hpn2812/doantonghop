package ute.udn.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "contentVocabulary")
public class ContentVocabulary implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long cVocabId;
	
	private String vocab;
	
	private String audio;
	
	private String images;
	
	@Column(columnDefinition="TEXT")
	private String mean;
	
	@Column(columnDefinition="TEXT")
	private String sentence;
	
	private String spelling;
	
	@ManyToOne
	@JoinColumn(name = "vocabId")
	private Vocabulary vocabulary;

}
