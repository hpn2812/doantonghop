package ute.udn.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "listening")
public class ListeningExcercise  implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long listenId;
	
	private String name;
	
	private int level;
	
	private int part;
	
	private String image;
	
	@OneToMany(mappedBy = "excercise", cascade = CascadeType.ALL)
	private List<ContentListeningExcercise> contentListeningExcercises;
	
	@ManyToOne
	@JoinColumn(name = "id")
	private User users;

}
