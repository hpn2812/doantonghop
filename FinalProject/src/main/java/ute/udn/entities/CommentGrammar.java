package ute.udn.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "commentgrammar")
public class CommentGrammar implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long cGramId;
	
	private String content;
	
	private int newComment;
	
	private int display;
	
	@Column(name = "time_comment")
	private Date timeComment;
 
	@ManyToOne
	@JoinColumn(name = "grammarId")
	private Grammar grammar;
	
	@ManyToOne
	@JoinColumn(name = "id")
	private User users;
}
