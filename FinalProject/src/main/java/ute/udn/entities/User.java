package ute.udn.entities;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users",uniqueConstraints = @UniqueConstraint(columnNames = "username") )
public class User implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String username;
	
	private String password;

	private String fullName;

	private String address;

	private String phoneNumber;
	private boolean enabled;

	
	@OneToMany(mappedBy = "users", cascade = CascadeType.ALL)
	private List<CommentGrammar> commentGrammars;
	
	@OneToMany(mappedBy = "users", cascade = CascadeType.ALL)
	private List<TestResult> testResult ;
	
	@OneToMany(mappedBy = "users", cascade = CascadeType.ALL)
	private List<Grammar> grammar ;
	
	@OneToMany(mappedBy = "users", cascade = CascadeType.ALL)
	private List<Vocabulary> vocabulary ;
	
	@OneToMany(mappedBy = "users", cascade = CascadeType.ALL)
	private List<ListeningExcercise> listeningExcercise ;
	
	@OneToMany(mappedBy = "users", cascade = CascadeType.ALL)
	private List<ReadingExcercise> readingExcercise ;
	
	@OneToMany(mappedBy = "users", cascade = CascadeType.ALL)
	private List<TestExam> testExam ;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(
			name = "users_roles",
			joinColumns = @JoinColumn(name = "user_id"),
			inverseJoinColumns = @JoinColumn(name = "role_id")
			)
	private Set<Role> roles;
	


}
