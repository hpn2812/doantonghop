package ute.udn.entities;


import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "test_exam")
public class TestExam  implements Serializable{
		
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private Long testExamId;
		
		private String name;
		
		private String image;
		
		
		@OneToMany(mappedBy = "testExams", cascade = CascadeType.ALL)
		private List<TestExamQuestion> testExamQuestions;
		
		@OneToMany(mappedBy = "testExam", cascade = CascadeType.ALL)
		private List<TestResult> testResult;
		
		@ManyToOne
		@JoinColumn(name = "id")
		private User users;
}
