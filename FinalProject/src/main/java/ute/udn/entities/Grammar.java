package ute.udn.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "grammars")
public class Grammar implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long grammarId;
	
	private String name;
	
	@Column(columnDefinition="TEXT")
	private String content;
	private String image;
	
	@OneToMany(mappedBy = "grammar",cascade = CascadeType.ALL)
	private List<CommentGrammar> commentGrammars;
	
	@ManyToOne
	@JoinColumn(name = "id")
	private User users;
	
	

}
