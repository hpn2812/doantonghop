package ute.udn.entities;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "reading")
public class ReadingExcercise implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long readId;
	@Column(nullable = true,length = 100)
	private String name;
	private int level;
	private int part;
	private String image;
	
	@OneToMany(mappedBy = "readingExcercise",cascade = CascadeType.ALL)
	private List<ContentReadingExcercise> contentReadingExcercises;
	
	@ManyToOne
	@JoinColumn(name = "id")
	private User users;
	

}
