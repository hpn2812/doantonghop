package ute.udn.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InfomationDto {
	
	
	private Long id;
	private String logoAdmin;
	private String logoUser;
	private String slide1;
	private String slide2;
	private String slide3;
	private int view;
}
