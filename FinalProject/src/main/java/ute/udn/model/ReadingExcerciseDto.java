package ute.udn.model;



import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReadingExcerciseDto {

	private Long readId;
	@NotEmpty
	@Column(length = 100)
	private String name;
	private int level;
	private int part;
	private String image;
	
	private Boolean isEdit=false;
	

	

}
