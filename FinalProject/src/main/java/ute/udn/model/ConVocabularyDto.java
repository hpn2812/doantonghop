package ute.udn.model;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ute.udn.entities.Vocabulary;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConVocabularyDto {
	
	private Long cVocabId;
	
	@NotEmpty
	private String vocab;
	
	private String audio;

	private String images;
	
	@NotEmpty
	private String mean;
	
	@NotEmpty
	private String sentence;
	
	@NotEmpty
	private String spelling;
	
	private Vocabulary vocabulary;
	
	private Boolean isEdit=false;

}
