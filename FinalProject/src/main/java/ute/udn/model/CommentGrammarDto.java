package ute.udn.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ute.udn.entities.Grammar;
import ute.udn.entities.User;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentGrammarDto {
	

	private Long cGramId;
	
	private String content;
	
	private int totalNewComment;
	
	private int display;
	
	private Date timeComment;
 
	
	private Grammar grammar;
	
	private User users;

}
