package ute.udn.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ListeningExcerciseDto implements Serializable{

	private Long listenId;
	
	@NotEmpty
	private String name;
	
	private int level;
	private int part;
	private String image;	
	private Boolean isEdit=false;


}
