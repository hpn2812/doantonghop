package ute.udn.model;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GrammarDto {
	
	private Long grammarId;
	
	@NotEmpty
	private String name;
	
	@NotEmpty
	private String content;
	
	private String image;
	
	
	private Boolean isEdit=false;

}
