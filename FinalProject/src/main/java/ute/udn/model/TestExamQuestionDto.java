package ute.udn.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.Type;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ute.udn.entities.TestExam;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TestExamQuestionDto{
	
	
	private Long questionId;
	private String audio;
	private String image;
	private String question;
	
	@NotEmpty
	private String option1;
	
	@NotEmpty
	private String option2;
	
	@NotEmpty
	private String option3;
	
	private String option4;
	private String pragraph;
	
	@NotEmpty
	private String corectOption;
	private String explains;

	private TestExam testExams;
	
	private Boolean isEdit=false;

}
