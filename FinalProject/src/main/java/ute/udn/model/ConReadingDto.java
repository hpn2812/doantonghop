package ute.udn.model;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ute.udn.entities.ReadingExcercise;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConReadingDto {

	private Long crId;
	
	@NotEmpty
	private String question;
	
	@NotEmpty
	private String correctReading;
	
	@NotEmpty
	private String a;
	
	@NotEmpty
	private String b;
	
	@NotEmpty
	private String c;
	
	private String d;
	
	private String explains;
	
	
	private String image;
	
	private String paragrahps;
	
	private ReadingExcercise readingExcercise;
	private Boolean isEdit=false;
	
	
	
}
