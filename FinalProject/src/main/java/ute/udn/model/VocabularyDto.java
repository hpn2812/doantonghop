package ute.udn.model;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VocabularyDto {
	
	private Long vocabId;
	private String image;
	
	@NotEmpty
	private String name;
	
	private Boolean isEdit=false;

}
