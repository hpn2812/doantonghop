package ute.udn.model;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ute.udn.entities.ListeningExcercise;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConListeningDto {
	
	private Long cListentId;
	private String question;
	@Length(max = 1)
	@NotEmpty
	private String correctOption;
	
	@NotEmpty
	private String option1;
	
	
	@NotEmpty
	private String option2;
	
	
	@NotEmpty
	private String option3;
	
	
	private String option4;
	private String explains;
	private String image;
	private String audio;
	private String stt;
	private ListeningExcercise excercise;
	private Boolean isEdit=false;

}
