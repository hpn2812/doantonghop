package ute.udn.model;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ute.udn.entities.TestResult;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class TestExamDto {
	
	private Long testExamId;
	
	@NotEmpty
	private String name;
	
	private String image;
	
	private TestResult testResult;
	
	private Boolean isEdit=false;
	
	
	 
	
	
	
}
