package ute.udn.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
	
	private Long id;
	
	@NotEmpty
	@Column(unique = true, length = 30,nullable = false)
	private String username;
	
	@NotEmpty
	@Column( length = 255)
	@Length(min = 5)
	private String password;
	
	@NotEmpty
	@Column(length = 255)
	private String fullName;
	
	
	@NotEmpty
	@Column( length = 12)
	@Length(min = 10,max =12 )
	private String phoneNumber;
	
	
	@NotEmpty
	@Column( length = 255)
	private String address;
	
	private Boolean enabled=true;
	
	private Boolean isEdit=false;
}
