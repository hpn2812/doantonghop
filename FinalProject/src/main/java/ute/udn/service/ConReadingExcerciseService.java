package ute.udn.service;

import java.util.List;

import org.springframework.data.domain.Page;

import ute.udn.entities.ContentListeningExcercise;
import ute.udn.entities.ContentReadingExcercise;
import ute.udn.entities.QuestionReadingForm;

public interface ConReadingExcerciseService {

	ContentReadingExcercise save(ContentReadingExcercise entity);

	List<ContentReadingExcercise> getListReadingQuestionByReadingId(long read_id);

	void deleteById(Long id);

	ContentReadingExcercise getReadingQuestionById(Long id);

	List<ContentReadingExcercise> findAll();

	QuestionReadingForm getQuestions(long read_id);

	int getResult(QuestionReadingForm qForm);

	Page<ContentReadingExcercise> pageable(long read_id, int pageNo, int pageSize);
	

	



}
