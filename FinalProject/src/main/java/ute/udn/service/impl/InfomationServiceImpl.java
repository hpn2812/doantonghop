package ute.udn.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ute.udn.entities.Infomation;
import ute.udn.repositories.InfomationRepository;
import ute.udn.service.InfomationService;

@Service
public class InfomationServiceImpl  implements InfomationService{

	@Autowired
	InfomationRepository repository;

	@Override
	public Infomation save(Infomation entity) {
		return repository.save(entity);
	}
	
	
	
	
}
