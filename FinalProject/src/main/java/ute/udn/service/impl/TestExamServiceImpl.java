package ute.udn.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ute.udn.entities.Grammar;
import ute.udn.entities.ListeningExcercise;
import ute.udn.entities.TestExam;
import ute.udn.entities.Vocabulary;
import ute.udn.repositories.TestExamRepository;
import ute.udn.service.TestExamService;

@Service
public class TestExamServiceImpl implements TestExamService {

	@Autowired
	TestExamRepository repository;

	@Override
	public List<TestExam> findAll() {
		return repository.findAll();
	}

	@Override
	public TestExam save(TestExam entity) {
		return repository.save(entity);
	}

	@Override
	public void deleteById(Long id) {
		repository.deleteById(id);
	}

	@Override
	public TestExam getTestExamById(Long id) {
		Optional<TestExam> optional = repository.findById(id);
		TestExam testexam = null;
		if (optional.isPresent()) {
			testexam = optional.get();
		} else {
			throw new RuntimeException(" Test exam not found for id :: " + id);
		}
		return testexam;
	}

	@Override
	public Page<TestExam> searchPageableByUserId(long id, int pageNo, int pageSize, String sortField,
			String sortDirection, String searchName) {

		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :

				Sort.by(sortField).descending();

		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);

		if ("".equalsIgnoreCase(searchName)) {
			return repository.findByUsers_Id(id, pageable);
		} else {
			return repository.findByNameContainingAndUsers_Id(searchName, id, pageable);
		}
	}

	@Override
	public Page<TestExam> searchPageableTestExam(int pageNo, int pageSize, String searchName) {
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
		if ("".equalsIgnoreCase(searchName)) {
			return repository.findAll(pageable);
		} else {
			return repository.findByNameContaining(searchName, pageable);
		}
	}

	@Override
	public int countTestExamByUserId(long id) {
		return repository.countTestExamByUserId(id);
	}
	

}
