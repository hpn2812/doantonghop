package ute.udn.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ute.udn.entities.ContentListeningExcercise;
import ute.udn.entities.ListeningExcercise;
import ute.udn.entities.ReadingExcercise;
import ute.udn.repositories.ReadingExRepository;
import ute.udn.service.ReadingExcerciseService;

@Service
public class ReadingExcerciseServiceImpl implements ReadingExcerciseService {

	@Autowired
	ReadingExRepository repository;

	@Override
	public ReadingExcercise save(ReadingExcercise entity) {
		return repository.save(entity);
	}
	
	@Override
	public String findByName(long id) {
		return repository.findByName(id);
	}

	@Override
	public List<ReadingExcercise> findAll() {
		return repository.findAll();
	}

	@Override
	public ReadingExcercise getReadingExcersiceById(Long id) {
		Optional<ReadingExcercise> optional = repository.findById(id);
		ReadingExcercise reading = null;
		if (optional.isPresent()) {
			reading = optional.get();
		} else {
			throw new RuntimeException(" ListeningExcercise not found for id :: " + id);
		}
		return reading;
	}

	@Override
	public Page<ReadingExcercise> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending()
				: Sort.by(sortField).descending();

		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
		return repository.findAll(pageable);
	}

	@Override
	public void deleteById(Long id) {
		repository.deleteById(id);
	}

	@Override
	public Optional<ReadingExcercise> findById(Long id) {
		return repository.findById(id);
	}

	@Override
	public Page<ReadingExcercise> searchPageableByUserId(Long id,int pageNo, int pageSize, String sortField, String sortDirection,
			int level,int part) {

		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :

				Sort.by(sortField).descending();

		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);

		if (part == 0 && level == 0) {
			return repository.findByUsers_Id(id, pageable);
		} else if (part != 0 && level == 0) {
			return repository.findByPartAndUsers_Id(part,id, pageable);
		} else if (part == 0 && level != 0) {
			return repository.findByLevelAndUsers_Id(level,id, pageable);
		} else {
			return repository.findByLevelAndPartAndUsers_Id(level, part,id, pageable);
		}

	}

	@Override
	public Page<ReadingExcercise> searchPageableUser(int pageNo, int pageSize, int part, int level) {
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
		if (part == 0 && level == 0) {
			return repository.findAll(pageable);
		} else if (part != 0 && level == 0) {
			return repository.findByPart(part, pageable);
		} else if (part == 0 && level != 0) {
			return repository.findByLevel(level, pageable);
		} else {
			return repository.findByLevelAndPart(part, level, pageable);
		}

	}

	@Override
	public int countReadingExcerciseByUserId(long id) {
		return repository.countReadingExcerciseByUserId(id);
	}
	
	
	

}
