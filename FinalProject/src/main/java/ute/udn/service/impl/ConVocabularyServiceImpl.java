package ute.udn.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ute.udn.entities.ContentListeningExcercise;
import ute.udn.entities.ContentVocabulary;
import ute.udn.entities.ListeningExcercise;
import ute.udn.repositories.ConVocabRepository;
import ute.udn.service.ConVocabularyService;

@Service
public class ConVocabularyServiceImpl  implements ConVocabularyService{
	
	@Autowired
	ConVocabRepository repository;

	@Override
	public ContentVocabulary save(ContentVocabulary entity) {
		return repository.save(entity);
	}

	@Override
	public List<ContentVocabulary> findAll() {
		return repository.findAll();
	}

	@Override
	public List<ContentVocabulary> getListContenVocabularyByVocabId(long vocab_id) {
		return repository.getListContenVocabularyByVocabId(vocab_id);
	}

	@Override
	public void deleteById(Long id) {
		repository.deleteById(id);
	}
	
	@Override
	public ContentVocabulary getVocabularyById(Long id) {
		Optional<ContentVocabulary> optional = repository.findById(id);
		ContentVocabulary vocab = null;
		if (optional.isPresent()) {
			vocab = optional.get();
		} else {
			throw new RuntimeException("Vocabulary not found for id :: " + id);
		}
		return vocab;
	}
	
	@Override
	public Page<ContentVocabulary> pageable(int pageNo, int pageSize,Long vocab_id) {
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
		return repository.getPageableContenVocabularyByVocabId(vocab_id, pageable);
	}
	

}
