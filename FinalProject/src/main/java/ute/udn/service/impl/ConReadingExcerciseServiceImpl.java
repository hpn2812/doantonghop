package ute.udn.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ute.udn.entities.ContentListeningExcercise;
import ute.udn.entities.ContentReadingExcercise;
import ute.udn.entities.QuestionReadingForm;
import ute.udn.repositories.ConReadingExcerciseRepository;
import ute.udn.service.ConReadingExcerciseService;

@Service
public class ConReadingExcerciseServiceImpl implements ConReadingExcerciseService {

	@Autowired
	ConReadingExcerciseRepository repository;
	
	@Autowired
	QuestionReadingForm qForm;

	@Override
	public ContentReadingExcercise save(ContentReadingExcercise entity) {
		return repository.save(entity);
	}

	@Override
	public List<ContentReadingExcercise> findAll() {
		return repository.findAll();
	}

	@Override
	public List<ContentReadingExcercise> getListReadingQuestionByReadingId(long read_id) {
		return repository.getListReadingQuestionByReadingId(read_id);
	}

	@Override
	public void deleteById(Long id) {
		repository.deleteById(id);
		
	}
	
	@Override
	public ContentReadingExcercise getReadingQuestionById(Long id) {
		Optional<ContentReadingExcercise> optional = repository.findById(id);
		ContentReadingExcercise reading = null;
		if (optional.isPresent()) {
			reading = optional.get();
		} else {
			throw new RuntimeException(" ContentReadingExcercise not found for id :: " + id);
		}
		return reading;
	}
	

	@Override
	public QuestionReadingForm getQuestions(long read_id) {
		
			List<ContentReadingExcercise> allQues =repository.getListReadingQuestionByReadingId(read_id);
			
			qForm.setQuestions(allQues);
			
			return qForm;
		}
	
	@Override
	public int getResult(QuestionReadingForm qForm) {
		int correct = 0;
		for(ContentReadingExcercise q: qForm.getQuestions())
			if(q.getCorrectReading().equalsIgnoreCase(q.getChose()))
				correct++;
		return correct;
	}
	
	
	@Override
	public Page<ContentReadingExcercise> pageable(long read_id,int pageNo, int pageSize) {
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
		return repository.getPageReadingQuestionByReadingId(pageable, read_id);
	}
		
	
	
}
