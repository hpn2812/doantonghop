package ute.udn.service.impl;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ute.udn.entities.HistoryExamTest;
import ute.udn.repositories.HistoryExamTestRepository;
import ute.udn.service.HistoryExamTestService;


@Service
public class HistoryExamTestServiceImpl implements HistoryExamTestService{
	
	@Autowired
	HistoryExamTestRepository repository;

	@Override
	public HistoryExamTest save(HistoryExamTest entity) {
		return repository.save(entity);
	}
	
	

	@Override
	public List<HistoryExamTest> getListHistoryListen(long id){
		List<HistoryExamTest> list= repository.getListHistoryByResultId(id);
		List<HistoryExamTest> list1=new ArrayList<HistoryExamTest>();
		
		for(int i=0; i<=49;++i) {
			list1.add(list.get(i));
		}
		
		return list1;

	}
	
	
	@Override
	public List<HistoryExamTest> getListHistoryReading(long id){
		
		List<HistoryExamTest> list= repository.getListHistoryByResultId(id);
		
		List<HistoryExamTest> list1=new ArrayList<HistoryExamTest>();
		for(int i=50; i<=99;++i) {
			list1.add(list.get(i));
		}
		
		return list1;

	}
	
	
	
	
}
