package ute.udn.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ute.udn.entities.CommentGrammar;
import ute.udn.entities.ListeningExcercise;
import ute.udn.entities.User;
import ute.udn.entities.Vocabulary;
import ute.udn.repositories.CommentGrammarRepository;
import ute.udn.service.CommentGrammarService;

@Service
public class CommentGrammarServiceImpl implements CommentGrammarService {

	@Autowired
	CommentGrammarRepository repository;

	@Override
	public  CommentGrammar save(CommentGrammar entity) {
		return repository.save(entity);
	}

	@Override
	public List<CommentGrammar> getListComments(long id) {
		return repository.findByDisplayAndGrammar_GrammarId(1, id);
	}

	@Override
	public long count() {
		return repository.count();
	}
	
	@Override
	public int countRecordComment(long id) {
		return repository.countRecordComment(id,1);
	}
	
	@Override
	public CommentGrammar getCommentById(Long id) {
		Optional<CommentGrammar> optional =repository.findById(id);
		CommentGrammar cmt = null;
		if (optional.isPresent()) {
			cmt = optional.get();
		} else {
			throw new RuntimeException(" user not found for id :: " + id);
		}
		return cmt;
	}
	
	@Override
	public Page<CommentGrammar> searchPageable(int pageNo, int pageSize, String sortField, String sortDirection,String content,int display,int newComment) {
		
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
			
		Sort.by(sortField).descending();
		
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
		
		if(display!=0 && newComment!=0 && "".equalsIgnoreCase(content)==false) {
			//th cả 3 đều nhập
			return repository.findByContentContainingAndDisplayAndNewComment(content, display, newComment, pageable);
		}else if(display!=0 && newComment==0 && "".equalsIgnoreCase(content) ) {
			//th chọn display ==>display!=0,newComment,content
			return repository.findByDisplay(display, pageable);
		}else if(display==0 && newComment!=0 && "".equalsIgnoreCase(content)) {
			//th chọn newComment ==>display,newComment!=0,content
			return repository.findByDisplay(newComment, pageable);
		}
		else if(display==0 && newComment==0 && "".equalsIgnoreCase(content)==false) {
			
			//th chọn content ==>display,newComment,content !=''
			return repository.findByContentContaining(content, pageable);
		}else if(display!=0 && newComment!=0 && "".equalsIgnoreCase(content)) {
			//th chọn display & newComment ==>display!=0,newComment !=0,content==''
			return repository.findByDisplayAndNewComment(display, newComment, pageable);
		}
		else if(display!=0 && newComment==0 && "".equalsIgnoreCase(content)==false) {
			//th chọn display & content ==>display!=0,newComment==0,content!=''
			return repository.findByDisplayAndContentContaining(display, content, pageable);
		}
		else if(display==0 && newComment!=0 && "".equalsIgnoreCase(content)==false) {
			//th chọn newComment & content ==>display==0,newComment!=0,content!=''
			return repository.findByNewCommentAndContentContaining(newComment, content, pageable);
		}
		else {
			//th cả 3 k nhập--> find all
			return repository.findAll(pageable);
		}
			

	}

	@Override
	public int countNewComment(int newComment) {
		return repository.countNewComment(newComment);
	}
	
	
	
}
