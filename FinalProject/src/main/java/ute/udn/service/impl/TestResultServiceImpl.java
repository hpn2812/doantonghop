package ute.udn.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ute.udn.entities.ListeningExcercise;
import ute.udn.entities.TestResult;
import ute.udn.entities.Vocabulary;
import ute.udn.repositories.TestResultRepository;
import ute.udn.service.TestResultService;

@Service
public class TestResultServiceImpl implements TestResultService {
	
	@Autowired
	TestResultRepository repository;

	@Override
	public TestResult save(TestResult entity) {
		return repository.save(entity);
	}
	
	
	@Override
	public int countTestResultOfWeek(long id) {
		return repository.countTestResultOfWeek(id);
	}

	@Override
	public TestResult getTestResultByUserId(long id) {
		return repository.getTestResultByUserId(id);
	}


	@Override
	public List<TestResult> findAll() {
		return repository.findAll();
	}


	@Override
	public List<TestResult> getTopScore() {
		List<TestResult> sList = repository.findAll(Sort.by(Sort.Direction.DESC, "totalCorrect"));
		return sList;
	}


	@Override
	public List<TestResult> getListTestResultOfTeacher(long id) {
		return repository.getListTestResultOfTeacher(id);
	}
	
	

	@Override
	public Page<TestResult> searchPageableUser(Long userIdUser,String  nameOfTestExam,int pageNo, int pageSize, String sortField, String sortDirection) {
		
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() : Sort.by(sortField).descending();
		
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
		
		if ("".equalsIgnoreCase(nameOfTestExam)) {
			return repository.getListTestResultByUserId(userIdUser, pageable);
		}else
		{
			return repository.getListTestResultByUserId(userIdUser,nameOfTestExam, pageable);
		}	
	
	}
	
	public Page<TestResult> getListTestResultByUserId(long id, String nameOfTestExam, Pageable pageable) {
		return repository.getListTestResultByUserId(id, nameOfTestExam, pageable);
	}


	@Override
	public Page<TestResult> searchPageable(Long userIdTeacher,String  username,int pageNo, int pageSize, String sortField, String sortDirection) {
		
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() : Sort.by(sortField).descending();
		
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
		
		if ("".equalsIgnoreCase(username)) {
			return repository.getPageTestResultOfTeacher(userIdTeacher, pageable);
		}else
		{
			return repository.getPageTestResultOfTeacher(userIdTeacher,username, pageable);
		}	
	
	}
	
}
