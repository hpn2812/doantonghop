package ute.udn.service.impl;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ute.udn.entities.Role;
import ute.udn.repositories.RoleRepository;
import ute.udn.service.RoleService;
@Service
public class RoleServiceImpl implements RoleService{
	@Autowired
	RoleRepository repository;

	@Override
	public List<Role> findByName(String name) {
		return (List<Role>) repository.findByName(name);
	}
	@Override
	public List<Role> getRole() {
		return repository.findAll();
	}
	 @Override
	public Set<Role> getListUserRoles(List<Long> listRoleId) {
	        return repository.findListByRoleId(listRoleId);
	    }
	 
	@Override
	public  Role save(Role entity) {
		return repository.save(entity);
	}
	
	
	@Override
	public Role getRoleById(Long id) {
		Optional<Role> optional = repository.findById(id);
		Role role = null;
		if (optional.isPresent()) {
			role = optional.get();
		} else {
			throw new RuntimeException(" Test exam not found for id :: " + id);
		}
		return role;
	}
	
	@Override
	public void deleteById(Long id) {
		repository.deleteById(id);
	}
	
}
