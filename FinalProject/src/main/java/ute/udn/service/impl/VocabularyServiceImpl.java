package ute.udn.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ute.udn.entities.Grammar;
import ute.udn.entities.ListeningExcercise;
import ute.udn.entities.ReadingExcercise;
import ute.udn.entities.Vocabulary;
import ute.udn.repositories.VocabularyRepository;
import ute.udn.service.VocabularyService;

@Service
public class VocabularyServiceImpl  implements VocabularyService{
	
	@Autowired
	VocabularyRepository repository;

	@Override
	public List<Vocabulary> findAll() {
		return repository.findAll();
	}
	
	
	@Override
	public Vocabulary save(Vocabulary entity) {
		return repository.save(entity);
	}


	@Override
	public Page<Vocabulary> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
			Sort.by(sortField).descending();
		
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
		return repository.findAll(pageable);
	}


	@Override
	public void deleteById(Long id) {
		repository.deleteById(id);
	}
	
	@Override
	public Vocabulary getVocabularyById(Long id) {
		Optional<Vocabulary> optional = repository.findById(id);
		Vocabulary vocab = null;
		if (optional.isPresent()) {
			vocab = optional.get();
		} else {
			throw new RuntimeException(" Vocabulary not found for id :: " + id);
		}
		return vocab;
	}
	
	

	@Override
	public Page<Vocabulary> searchPageableByUserId(Long userId,int pageNo, int pageSize, String sortField, String sortDirection,String searchName) {
		
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() : Sort.by(sortField).descending();
		
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
		
		
		if("".equalsIgnoreCase(searchName)) {
			return repository.findByUsers_Id(userId, pageable);
		}
		else {
			return repository.findByNameContainingAndUsers_Id(searchName,userId, pageable);
		}
	}
	
		@Override
		public Page<Vocabulary> searchPageableVocabulary(int pageNo, int pageSize,String searchName) {
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
		if("".equalsIgnoreCase(searchName)) {
			return repository.findAll(pageable);
		}
		else {
			return repository.findByNameContaining(searchName, pageable);
		}
	}


		@Override
		public int countVocabularyByUserId(long id) {
			return repository.countVocabularyByUserId(id);
		}
	

}
