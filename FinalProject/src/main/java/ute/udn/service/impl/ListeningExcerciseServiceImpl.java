package ute.udn.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ute.udn.entities.ContentListeningExcercise;
import ute.udn.entities.ListeningExcercise;
import ute.udn.entities.QuestionListenForm;
import ute.udn.entities.User;
import ute.udn.repositories.ListeningExcerciseRepository;
import ute.udn.service.ListeningExcerciseService;

@Service
@Transactional
public class ListeningExcerciseServiceImpl implements ListeningExcerciseService {
	@Autowired
	ListeningExcerciseRepository repository;

	@Override
	public ListeningExcercise findByImage(String image) {
		return repository.findByImage(image);
	}

	@Override
	public List<ListeningExcercise> findAll() {
		return repository.findAll();
	}

	@Override
	public ListeningExcercise save(ListeningExcercise entity) {
		return repository.save(entity);
	}

	@Override
	public ListeningExcercise getListeningExcersiceById(Long id) {
		Optional<ListeningExcercise> optional = repository.findById(id);
		ListeningExcercise listening = null;
		if (optional.isPresent()) {
			listening = optional.get();
		} else {
			throw new RuntimeException(" ListeningExcercise not found for id :: " + id);
		}
		return listening;
	}

	@Override
	public void deleteById(Long id) {
		repository.deleteById(id);
	}
	
	@Override
	public Page<ListeningExcercise> searchPageableUser(int pageNo, int pageSize, int part, int level) {
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
		if (part == 0 && level == 0) {
			return repository.findAll(pageable);
		} else if (part != 0 && level == 0) {
			return repository.findByPart(part, pageable);
		}else if (part == 0 && level != 0){
			return repository.findByLevel(level, pageable);
		}else {
			return repository.findByLevelAndPart(part, level, pageable);
		}
		
	}



	@Override
	public Page<ListeningExcercise> searchPageableByUserId(Long id,int pageNo, int pageSize, String sortField, String sortDirection, int level, int part) {
			
			Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
				
			Sort.by(sortField).descending();
			
			Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);

			if (part == 0 && level == 0) {
				return repository.findByUsers_Id(id, pageable);
			} else if (part != 0 && level == 0) {
				return repository.findByPartAndUsers_Id(part,id, pageable);
			}else if (part == 0 && level != 0){
				return repository.findByLevelAndUsers_Id( level,id, pageable);
			}else {
				return repository.findByLevelAndPartAndUsers_Id( part, level,id, pageable);
			}
			
		}

	@Override
	public int countListenExcerciseByUserId(long id) {
		return repository.countListenExcerciseByUserId(id);
	}
	
	
	
	

	

}
