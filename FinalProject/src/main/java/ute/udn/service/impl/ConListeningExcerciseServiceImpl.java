package ute.udn.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ute.udn.entities.ContentListeningExcercise;
import ute.udn.entities.ListeningExcercise;
import ute.udn.entities.QuestionListenForm;
import ute.udn.repositories.ConListeningExcerciseRepository;
import ute.udn.repositories.ListeningExcerciseRepository;
import ute.udn.service.ConListeningExcerciseService;
@Service
public class ConListeningExcerciseServiceImpl implements ConListeningExcerciseService {

	@Autowired
	ConListeningExcerciseRepository repository;
	
	@Autowired
	ListeningExcerciseRepository listenRepository;
	
	@Autowired
	QuestionListenForm qForm;

	
	@Override
	public ContentListeningExcercise  save(ContentListeningExcercise entity) {
		
		return repository.save(entity);
	}


	@Override
	public void deleteById(Long id) {
		Optional<ContentListeningExcercise> optional = repository.findById(id);
		if(optional.isPresent()) {
			repository.deleteById(id);
		}
		else {
			throw new RuntimeException(" ListeningExcercise not found for id :: " + id);
		}
		
	}
	
	
	//random question
	@Override
	public QuestionListenForm getQuestions(long listen_id) {
			List<ContentListeningExcercise> allQues =repository.getListListenQuestionByListenId(listen_id);
			qForm.setQuestions(allQues);
			return qForm;
		}

	
	@Override
	public List<ContentListeningExcercise> findAll() {
		return repository.findAll();
	}


	@Override
	public ContentListeningExcercise findByAudio(String audio) {
		return repository.findByAudio(audio);
	}


	@Override
	public List<ContentListeningExcercise> getListListenQuestionByListenId(long listen_id) {
		return repository.getListListenQuestionByListenId(listen_id);
	}
	


	@Override
	public ContentListeningExcercise getListeningQuestionById(Long id) {
		Optional<ContentListeningExcercise> optional = repository.findById(id);
		ContentListeningExcercise listening = null;
		if (optional.isPresent()) {
			listening = optional.get();
		} else {
			throw new RuntimeException(" ListeningExcercise not found for id :: " + id);
		}
		return listening;
	}
	

	@Override
	public int getResult(QuestionListenForm qForm) {
		
		int correct = 0;
		for(ContentListeningExcercise q: qForm.getQuestions())
			if(q.getCorrectOption().equalsIgnoreCase(q.getChose()))
				correct++;
		return correct;
	}
	
	
	

	@Override
	public Page<ContentListeningExcercise> pageable(long listen_id,int pageNo, int pageSize) {
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
		return repository.getPageListenQuestionByListenId(pageable,listen_id);
	}
	
}
