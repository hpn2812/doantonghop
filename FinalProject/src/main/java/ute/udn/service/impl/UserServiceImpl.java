package ute.udn.service.impl;



import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ute.udn.entities.User;
import ute.udn.model.UserDto;
import ute.udn.repositories.UserRepository;
import ute.udn.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService{
	@Autowired
	private UserRepository repository;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder ;

	
	
	@Override
	public User saveAndFlush(User user) {
		return repository.saveAndFlush(user);
	}

	@Override
	public User save(User user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		return repository.save(user);
	}
	
	@Override
	public User updateUser(User user) {
		return repository.save(user);
	}


	@Override
	public User findByUsernameLike(String userName) {
		return repository.findByUsernameLike(userName);
	}
	
	


	@Override
	public List<User> findAll() {
		return repository.findAll();
	}


	@Override
	public User getUserById(Long id) {
		Optional<User> optional =repository.findById(id);
		User user = null;
		if (optional.isPresent()) {
			user = optional.get();
		} else {
			throw new RuntimeException(" user not found for id :: " + id);
		}
		return user;
	}
	
	@Override
	public List<String> findListRolesOfUser(long userId) {
		return repository.findListRolesOfUser(userId);
	}
	

	@Override
	public Page<User> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
		
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
			
		Sort.by(sortField).descending();
		
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
		
		/*
		 * if(role==0) { return repository.searchAndPageable(role, pageable); }
		 */
		
		return repository.findAll(pageable);
	}
	
	@Override
	public Page<User> searchPageable(int pageNo, int pageSize, String sortField, String sortDirection,Long role) {
		
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :
			
		Sort.by(sortField).descending();
		
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);
		

		if(role!=0) { 
			return repository.searchAndPageable(role, pageable); 
		}

		return repository.findAll(pageable);
	}

	
	

	@Override
	public int countUserByRole(long role) {
		return repository.countUser(role);
	}

	@Override
	public List<User> search(long role) {
		long a=1,b=2;
		if(role==a || role==b) {
			return repository.search(role);
		}
		else {
			return repository.findAll();
		}
		
	}

}
