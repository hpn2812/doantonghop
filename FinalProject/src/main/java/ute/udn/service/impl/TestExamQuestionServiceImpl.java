package ute.udn.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import ute.udn.entities.ContentListeningExcercise;
import ute.udn.entities.ContentReadingExcercise;
import ute.udn.entities.QuestionListenForm;
import ute.udn.entities.QuestionTestExamForm;
import ute.udn.entities.TestExam;
import ute.udn.entities.TestExamQuestion;
import ute.udn.repositories.TestExamQuestionRepository;
import ute.udn.repositories.TestExamRepository;
import ute.udn.service.TestExamQuestionService;

@Service
public class TestExamQuestionServiceImpl implements TestExamQuestionService{
	
	@Autowired
	TestExamQuestionRepository repository;
	


	
	@Override
	public TestExamQuestion save(TestExamQuestion entity) {
		return repository.save(entity);
	}
	
	@Override
	public List<TestExamQuestion> getListTestExamQuestionById(long testExamId) {
		return repository.getListTestExamQuestionById(testExamId);
	}
	
	
	@Override
	public QuestionTestExamForm getQuestions(long id) {
		QuestionTestExamForm qForm= new QuestionTestExamForm();
		List<TestExamQuestion> allQues =repository.getListTestExamQuestionById(id);
		List<TestExamQuestion> qList = new ArrayList<TestExamQuestion>();
		
//		Random random = new Random();
//		for(int i=0; i<3; i++) {
//			int rand = random.nextInt(allQues.size());
//			qList.add(allQues.get(rand));
//			allQues.remove(rand);
//		}
		for(int i=0; i<=49;++i) {
			qList.add(allQues.get(i));
		}
		qForm.setQuestions(qList);
		return qForm;
	}
	
	@Override
	public QuestionTestExamForm getQuestionReading(long id) {
		QuestionTestExamForm qForm= new QuestionTestExamForm();
		List<TestExamQuestion> allQues =repository.getListTestExamQuestionById(id);
		List<TestExamQuestion> qList = new ArrayList<TestExamQuestion>();
		
		
//		Random random = new Random();
//		for(int i=50; i<=99; i++) {
//			int rand = random.nextInt(allQues.size());
//			qList.add(allQues.get(rand));
//			allQues.remove(rand);
//		}
		for(int i=50; i<=99;++i) {
			qList.add(allQues.get(i));
		}
		
		qForm.setQuestions(qList);
		return qForm;
	}
	
	@Override
	public int getResult(QuestionTestExamForm qForm) {
		int correct = 0;
		for(TestExamQuestion q: qForm.getQuestions())
			if(q.getCorectOption().equalsIgnoreCase(q.getChose()))
				correct++;
		return correct;
	}
	
	
	
	@Override
	public Page<TestExamQuestion> pageable(long id,int pageNo, int pageSize) {
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
		return repository.getPageTestExamQuestionById(id, pageable);
	}
	
	@Override
	public TestExamQuestion getTestQuestionById(Long id) {
		Optional<TestExamQuestion> optional = repository.findById(id);
		TestExamQuestion test = null;
		if (optional.isPresent()) {
			test = optional.get();
		} else {
			throw new RuntimeException(" TestExamQuestion not found for id :: " + id);
		}
		return test;
	}

	@Override
	public void deleteById(Long id) {
		repository.deleteById(id);
	}
	
}
