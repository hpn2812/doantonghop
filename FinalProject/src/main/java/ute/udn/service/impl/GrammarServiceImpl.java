package ute.udn.service.impl;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import ute.udn.entities.Grammar;
import ute.udn.repositories.GrammarRepository;
import ute.udn.service.GrammarService;

@Service
public class GrammarServiceImpl implements GrammarService {

	@Autowired
	GrammarRepository repository;

	@Override
	public void deleteById(Long id) {
		repository.deleteById(id);
	}

	@Override
	public Grammar save(Grammar entity) {
		return repository.save(entity);
	}

	@Override
	public Grammar getGrammarById(Long id) {
		Optional<Grammar> optional = repository.findById(id);
		Grammar gr = null;
		if (optional.isPresent()) {
			gr = optional.get();
		} else {
			throw new RuntimeException(" Grammar not found for id :: " + id);
		}
		return gr;
	}

	@Override
	public int countGrammarByUserId(long id) {
		return repository.countByGrammarId(id);
	}

	@Override
	public Page<Grammar> searchPageableByUserId(Long userId,int pageNo, int pageSize, String sortField, String sortDirection,
			String searchName) {

		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending() :

				Sort.by(sortField).descending();

		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);

		if ("".equalsIgnoreCase(searchName)) {
			return repository.findByUsers_Id(userId,pageable);
		} else {
			return repository.findByNameContainingAndUsers_Id(searchName,userId, pageable);
		}
	}

	@Override
	public Page<Grammar> searchPageableGrammar(int pageNo, int pageSize, String searchName) {
		Pageable pageable = PageRequest.of(pageNo - 1, pageSize);
		if ("".equalsIgnoreCase(searchName)) {
			return repository.findAll(pageable);
		} else {
			return repository.findByNameContaining(searchName, pageable);
		}
	}

}
