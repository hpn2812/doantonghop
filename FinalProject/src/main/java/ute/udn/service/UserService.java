package ute.udn.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import ute.udn.entities.User;
import ute.udn.model.UserDto;

public interface UserService {

	User save(User entity);

	User saveAndFlush(User user);

	User findByUsernameLike(String userName);

	List<User> findAll();

	User getUserById(Long id);

	User updateUser(User user);

	List<String> findListRolesOfUser(long userId);

	List<User> search(long role);

	Page<User> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

	Page<User> searchPageable(int pageNo, int pageSize, String sortField, String sortDirection, Long role);

	int countUserByRole(long role);



}
