package ute.udn.service;

import java.util.List;

import org.springframework.data.domain.Page;

import ute.udn.entities.Vocabulary;

public interface VocabularyService {

	List<Vocabulary> findAll();

	Page<Vocabulary> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

	Vocabulary save(Vocabulary entity);

	void deleteById(Long id);

	Vocabulary getVocabularyById(Long id);

	Page<Vocabulary> searchPageableVocabulary(int pageNo, int pageSize, String searchName);

	Page<Vocabulary> searchPageableByUserId(Long userId, int pageNo, int pageSize, String sortField, String sortDirection, String searchName);

	int countVocabularyByUserId(long id);

}
