package ute.udn.service;

import java.util.List;

import org.springframework.data.domain.Page;

import ute.udn.entities.ContentVocabulary;

public interface ConVocabularyService {

	List<ContentVocabulary> findAll();

	ContentVocabulary save(ContentVocabulary entity);

	List<ContentVocabulary> getListContenVocabularyByVocabId(long vocab_id);

	void deleteById(Long id);

	ContentVocabulary getVocabularyById(Long id);

	Page<ContentVocabulary> pageable(int pageNo, int pageSize, Long vocab_id);

}
