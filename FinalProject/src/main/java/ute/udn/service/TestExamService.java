package ute.udn.service;

import java.util.List;

import org.springframework.data.domain.Page;

import ute.udn.entities.TestExam;

public interface TestExamService {

	List<TestExam> findAll();

	TestExam save(TestExam entity);

	void deleteById(Long id);

	TestExam getTestExamById(Long id);

	Page<TestExam> searchPageableTestExam(int pageNo, int pageSize, String searchName);

	Page<TestExam> searchPageableByUserId(long id, int pageNo, int pageSize, String sortField, String sortDirection, String searchName);

	int countTestExamByUserId(long id);

}
