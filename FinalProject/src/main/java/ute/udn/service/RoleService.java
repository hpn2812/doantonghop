package ute.udn.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import ute.udn.entities.Role;

public interface RoleService {

	List<Role> findByName(String name);

	List<Role> getRole();

	Set<Role> getListUserRoles(List<Long> listRoleId);

	Role save(Role entity);

	Role getRoleById(Long id);

	void deleteById(Long id);
	
	
}
