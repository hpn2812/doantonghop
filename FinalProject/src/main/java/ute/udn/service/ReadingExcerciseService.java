package ute.udn.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import ute.udn.entities.ReadingExcercise;

public interface ReadingExcerciseService {

	ReadingExcercise save(ReadingExcercise entity);

	List<ReadingExcercise> findAll();

	ReadingExcercise getReadingExcersiceById(Long id);

	void deleteById(Long id);

	Page<ReadingExcercise> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

	Optional<ReadingExcercise> findById(Long id);

	Page<ReadingExcercise> searchPageableUser(int pageNo, int pageSize, int part, int level);

	Page<ReadingExcercise> searchPageableByUserId(Long id, int pageNo, int pageSize, String sortField, String sortDirection, int level,
			int part);

	int countReadingExcerciseByUserId(long id);

	String findByName(long id);

}
