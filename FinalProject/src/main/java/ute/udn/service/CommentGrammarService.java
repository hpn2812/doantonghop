package ute.udn.service;

import java.util.List;

import org.springframework.data.domain.Page;

import ute.udn.entities.CommentGrammar;

public interface CommentGrammarService {

	CommentGrammar save(CommentGrammar entity);

	List<CommentGrammar> getListComments(long id);

	long count();

	int countRecordComment(long id);

	Page<CommentGrammar> searchPageable(int pageNo, int pageSize, String sortField, String sortDirection, String content, int display, int newComment);

	CommentGrammar getCommentById(Long id);

	int countNewComment(int newComment);

}
