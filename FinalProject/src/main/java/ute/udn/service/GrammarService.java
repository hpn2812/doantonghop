package ute.udn.service;

import java.util.List;

import org.springframework.data.domain.Page;

import ute.udn.entities.Grammar;

public interface GrammarService {

	void deleteById(Long id);

	Grammar save(Grammar entity);

	Grammar getGrammarById(Long id);

	Page<Grammar> searchPageableGrammar(int pageNo, int pageSize, String searchName);

	Page<Grammar> searchPageableByUserId(Long userId, int pageNo, int pageSize, String sortField, String sortDirection, String searchName);

	int countGrammarByUserId(long id);



}
