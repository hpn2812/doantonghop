package ute.udn.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import ute.udn.entities.ListeningExcercise;

public interface ListeningExcerciseService {

	List<ListeningExcercise> findAll();

	ListeningExcercise save(ListeningExcercise entity);

	ListeningExcercise getListeningExcersiceById(Long id);
	
	void deleteById(Long id);

	ListeningExcercise findByImage(String image);

	Page<ListeningExcercise> searchPageableUser(int pageNo, int pageSize, int part, int level);

	Page<ListeningExcercise> searchPageableByUserId(Long id, int pageNo, int pageSize, String sortField, String sortDirection, int level,
			int part);

	int countListenExcerciseByUserId(long id);

	
}
