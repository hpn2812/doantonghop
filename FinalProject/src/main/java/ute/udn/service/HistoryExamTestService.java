package ute.udn.service;

import java.util.List;

import ute.udn.entities.HistoryExamTest;

public interface HistoryExamTestService {

	HistoryExamTest save(HistoryExamTest entity);

	List<HistoryExamTest> getListHistoryListen(long id);

	List<HistoryExamTest> getListHistoryReading(long id);

}
