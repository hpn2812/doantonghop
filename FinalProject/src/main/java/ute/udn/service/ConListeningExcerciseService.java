package ute.udn.service;

import java.util.List;

import org.springframework.data.domain.Page;

import ute.udn.entities.ContentListeningExcercise;
import ute.udn.entities.QuestionListenForm;
import ute.udn.model.ConListeningDto;

public interface ConListeningExcerciseService {

	ContentListeningExcercise save(ContentListeningExcercise entity);

	void deleteById(Long id);

	List<ContentListeningExcercise> findAll();

	ContentListeningExcercise findByAudio(String audio);

	List<ContentListeningExcercise> getListListenQuestionByListenId(long listen_id);

	ContentListeningExcercise getListeningQuestionById(Long id);

	QuestionListenForm getQuestions(long listen_id);

	int getResult(QuestionListenForm qForm);

	Page<ContentListeningExcercise> pageable(long listen_id, int pageNo, int pageSize);
}
