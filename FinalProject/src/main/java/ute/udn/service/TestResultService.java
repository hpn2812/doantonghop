package ute.udn.service;

import java.util.List;

import org.springframework.data.domain.Page;

import ute.udn.entities.TestResult;

public interface TestResultService {

	TestResult save(TestResult entity);

	List<TestResult> getTopScore();

	List<TestResult> findAll();

	TestResult getTestResultByUserId(long id);

	int countTestResultOfWeek(long id);

	List<TestResult> getListTestResultOfTeacher(long id);

	Page<TestResult> searchPageable(Long userIdTeacher, String  username, int pageNo, int pageSize, String sortField, String sortDirection);

	Page<TestResult> searchPageableUser(Long userIdUser, String  nameOfTestExam, int pageNo, int pageSize, String sortField, String sortDirection);


	

}
