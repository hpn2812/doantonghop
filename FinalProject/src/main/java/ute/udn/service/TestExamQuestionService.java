package ute.udn.service;

import java.util.List;

import org.springframework.data.domain.Page;

import ute.udn.entities.QuestionTestExamForm;
import ute.udn.entities.TestExamQuestion;

public interface TestExamQuestionService {

	TestExamQuestion save(TestExamQuestion entity);

	List<TestExamQuestion> getListTestExamQuestionById(long testExamId);

	QuestionTestExamForm getQuestions(long id);

	int getResult(QuestionTestExamForm qForm);

	QuestionTestExamForm getQuestionReading(long id);

	Page<TestExamQuestion> pageable(long id, int pageNo, int pageSize);

	TestExamQuestion getTestQuestionById(Long id);

	void deleteById(Long id);

}
