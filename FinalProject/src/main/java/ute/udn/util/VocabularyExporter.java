package ute.udn.util;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ute.udn.entities.ContentVocabulary;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class VocabularyExporter {
	
	 	private XSSFWorkbook workbook;
	    private XSSFSheet sheet;
	    private List<ContentVocabulary> listVocabulary;
	     
	    public VocabularyExporter(List<ContentVocabulary> listVocabulary) {
	        this.listVocabulary = listVocabulary;
	        workbook = new XSSFWorkbook();
	    }
	 
	 
	    private void writeHeaderLine() {
	        sheet = workbook.createSheet("Users");
	         
	        Row row = sheet.createRow(0);
	         
	        CellStyle style = workbook.createCellStyle();
	        XSSFFont font = workbook.createFont();
	        font.setBold(true);
	        font.setFontHeight(16);
	        style.setFont(font);
	         
	        createCell(row, 0, "ID", style);       
	        createCell(row, 1, "Từ vựng", style);    
	        createCell(row, 2, "Vần", style);
	        createCell(row, 3, "Nghĩa", style);
	        createCell(row, 4, "Câu", style);
	         
	    }
	     
	    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
	        sheet.autoSizeColumn(columnCount);
	        Cell cell = row.createCell(columnCount);
	        if (value instanceof Integer) {
	            cell.setCellValue((Integer) value);
	        } else if (value instanceof Boolean) {
	            cell.setCellValue((Boolean) value);
	        }else {
	            cell.setCellValue((String) value);
	        }
	        cell.setCellStyle(style);
	    }
	     
	    private void writeDataLines() {
	        int rowCount = 1;
	 
	        CellStyle style = workbook.createCellStyle();
	        XSSFFont font = workbook.createFont();
	        font.setFontHeight(14);
	        style.setFont(font);
	                 
	       

	        for (ContentVocabulary vocab : listVocabulary) {
	            Row row = sheet.createRow(rowCount++);
	            int columnCount = 0;
	            createCell(row, columnCount++, vocab.getCVocabId().toString(), style);
	            createCell(row, columnCount++, vocab.getVocab(), style);
	            createCell(row, columnCount++, vocab.getSpelling(), style);
	            createCell(row, columnCount++, vocab.getMean(), style);
	            createCell(row, columnCount++, vocab.getSentence(), style);
	             
	        }
	    }
	     
	    public void export(HttpServletResponse response) throws IOException {
	        writeHeaderLine();
	        writeDataLines();
	         
	        ServletOutputStream outputStream = response.getOutputStream();
	        workbook.write(outputStream);
	        workbook.close();
	         
	        outputStream.close();
	         
	    }

}
