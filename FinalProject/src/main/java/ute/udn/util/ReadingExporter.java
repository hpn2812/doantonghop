package ute.udn.util;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ute.udn.entities.ContentReadingExcercise;
import ute.udn.entities.ContentVocabulary;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReadingExporter {
	
	 	private XSSFWorkbook workbook;
	    private XSSFSheet sheet;
	    private List<ContentReadingExcercise> listcontentReading;
	     
	    public ReadingExporter(List<ContentReadingExcercise> listcontentReading) {
	        this.listcontentReading = listcontentReading;
	        workbook = new XSSFWorkbook();
	    }
	 
	 
	    private void writeHeaderLine() {
	        sheet = workbook.createSheet("Đề đọc");
	         
	        Row row = sheet.createRow(0);
	         
	        CellStyle style = workbook.createCellStyle();
	        XSSFFont font = workbook.createFont();
	        font.setBold(true);
	        font.setFontHeight(16);
	        style.setFont(font);
	         
	        createCell(row, 0, "ID", style);       
	        createCell(row, 1, "Đoạn văn", style);    
	        createCell(row, 2, "Câu hỏi", style);
	        createCell(row, 3, "Đáp án A", style);
	        createCell(row, 4, "Đáp án B", style);
	        createCell(row, 5, "Đáp án C", style);
	        createCell(row, 6, "Đáp án D", style);
	        createCell(row, 7, "Đáp án đúng", style);
	        createCell(row, 8, "Giải thích", style);
	         
	    }
	     
	    private void createCell(Row row, int columnCount, Object value, CellStyle style) {
	        sheet.autoSizeColumn(columnCount);
	        Cell cell = row.createCell(columnCount);
	        if (value instanceof Integer) {
	            cell.setCellValue((Integer) value);
	        } else if (value instanceof Boolean) {
	            cell.setCellValue((Boolean) value);
	        }else {
	            cell.setCellValue((String) value);
	        }
	        cell.setCellStyle(style);
	    }
	     
	    private void writeDataLines() {
	        int rowCount = 1;
	 
	        CellStyle style = workbook.createCellStyle();
	        XSSFFont font = workbook.createFont();
	        font.setFontHeight(14);
	        style.setFont(font);
	                 
	       

	        for (ContentReadingExcercise vocab : listcontentReading) {
	            Row row = sheet.createRow(rowCount++);
	            int columnCount = 0;
	             
	            createCell(row, columnCount++, vocab.getCrId().toString(), style);
	            createCell(row, columnCount++, vocab.getParagrahps(), style);
	            createCell(row, columnCount++, vocab.getQuestion(), style);
	            createCell(row, columnCount++, vocab.getA(), style);
	            createCell(row, columnCount++, vocab.getB(), style);
	            createCell(row, columnCount++, vocab.getC(), style);
	            createCell(row, columnCount++, vocab.getD(), style);
	            createCell(row, columnCount++, vocab.getCorrectReading(), style);
	            createCell(row, columnCount++, vocab.getExplains(), style);
	             
	        }
	    }
	     
	    public void export(HttpServletResponse response) throws IOException {
	        writeHeaderLine();
	        writeDataLines();
	         
	        ServletOutputStream outputStream = response.getOutputStream();
	        workbook.write(outputStream);
	        workbook.close();
	         
	        outputStream.close();
	         
	    }

}
