package ute.udn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class FinalProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinalProjectApplication.class, args);
//		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//	    String encoded = encoder.encode("admin");
//	    System.out.println("password is:"+encoded);
	}

}
