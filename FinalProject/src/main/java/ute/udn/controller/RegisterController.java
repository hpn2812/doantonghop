package ute.udn.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ch.qos.logback.core.joran.util.beans.BeanUtil;
import ute.udn.entities.Role;
import ute.udn.entities.User;
import ute.udn.model.UserDto;
import ute.udn.service.RoleService;
import ute.udn.service.UserService;

@Controller
public class RegisterController {

	@Autowired
	UserService userService;

	@Autowired
	RoleService roleService;

	@GetMapping("/registration")
	public String registration(Model model) {
		model.addAttribute("user", new UserDto());
		model.addAttribute("listUserRoleId", roleService.getRole());
		return "registration";
	}

	@PostMapping("/registration")
	public String saveOrUpdate(ModelMap model,
			@Valid @ModelAttribute("user") UserDto dto,
			BindingResult result) {

			User existing = userService.findByUsernameLike(dto.getUsername());
	        if (existing != null) {
	          model.addAttribute("error", "Tên đăng nhập đã tồn tại trên hệ thống");
	          return "registration";
	        }
	        
	        if (result.hasErrors()) {
				return "registration";
			}
	        
			long a = 2;
			List<Long> list = new ArrayList<Long>();
			list.add(a);
			dto.setEnabled(true);
			User user=new User();
			BeanUtils.copyProperties(dto, user);
			userService.save(user);
			user.setRoles(roleService.getListUserRoles(list));
			userService.saveAndFlush(user);
			model.addAttribute("message", "Đăng kí tài khoản thành công");
			return "registration";
			
		
	}

}
