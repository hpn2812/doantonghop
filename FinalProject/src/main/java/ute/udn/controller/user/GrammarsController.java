package ute.udn.controller.user;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ute.udn.entities.CommentGrammar;
import ute.udn.entities.Grammar;
import ute.udn.entities.User;
import ute.udn.model.CommentGrammarDto;
import ute.udn.service.CommentGrammarService;
import ute.udn.service.GrammarService;
import ute.udn.service.UserService;

@Controller
@RequestMapping("/grammar")
public class GrammarsController {

	@Autowired
	GrammarService service;

	@Autowired
	UserService userService;

	@Autowired
	CommentGrammarService commentService;

	@GetMapping("")
	public String home(ModelMap model) {

		return findPaginated(1, "", model);
	}

	@GetMapping("/{grammarId}")
	public String infoGrammar(ModelMap model, @PathVariable("grammarId") Long grammarId, HttpSession session,
			HttpServletRequest request) {
		model.addAttribute("count", commentService.countRecordComment(grammarId));
		model.addAttribute("comments", new CommentGrammarDto());
		model.addAttribute("detail", service.getGrammarById(grammarId));
		model.addAttribute("listComment", commentService.getListComments(grammarId));
		session.setAttribute("grammarId", grammarId);
		return "/user/grammar/content";
	}

	@PostMapping("/comment")
	public String saveComment(ModelMap model,
			@ModelAttribute("comments") CommentGrammarDto dto,
			BindingResult result,
			HttpSession session,
			HttpServletRequest request) {
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = principal.toString();

		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		}
		long grammarId=(long)request.getSession().getAttribute("grammarId");
		User currentUser = userService.findByUsernameLike(username);
		
		User userss= new User();
		userss.setId(currentUser.getId());
	
		Grammar grammar= new Grammar();
		grammar.setGrammarId(grammarId);
		
		CommentGrammar entity= new CommentGrammar();
		
		BeanUtils.copyProperties(dto, entity);
		entity.setDisplay(1);
		entity.setNewComment(1);
		entity.setTimeComment(new Date());
		entity.setUsers(userss);
		entity.setGrammar(grammar);
		
		commentService.save(entity);
		
		session.removeAttribute("grammarId");
		return "redirect:/grammar/"+grammarId;
	}

	@GetMapping("/page/{pageNo}")
	public String findPaginated(@PathVariable(value = "pageNo") int pageNo,
			@RequestParam("searchName") String searchName, ModelMap model) {
		int pageSize = 3;
		Page<Grammar> page = service.searchPageableGrammar(pageNo, pageSize, searchName);
		List<Grammar> grammar = page.getContent();
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("itemStart", pageNo * pageSize);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("grammar", grammar);
		model.addAttribute("searchName", searchName);
		return "/user/grammar/home";
	}

}
