package ute.udn.controller.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ute.udn.entities.ContentVocabulary;
import ute.udn.entities.ListeningExcercise;
import ute.udn.entities.Vocabulary;
import ute.udn.service.ConVocabularyService;
import ute.udn.service.VocabularyService;

@Controller
@RequestMapping("/vocabulary")
public class VocabularyController {
	
	@Autowired
	VocabularyService service;
	
	@Autowired
	ConVocabularyService conService;
	
	@GetMapping("")
	public String viewHomePage(ModelMap model) {
		return findPaginated(1,"",model);
	}
	
	@GetMapping("/{vocabId}")
	public String infoListen(ModelMap model, @PathVariable("vocabId") Long vocabId) {
		List<ContentVocabulary> cdto = conService.getListContenVocabularyByVocabId(vocabId);
		model.addAttribute("vocab", cdto);
		model.addAttribute("detail", service.getVocabularyById(vocabId));
		return "/user/vocabulary/content";
	}
	
	@GetMapping("/page/{pageNo}")
	public String findPaginated(@PathVariable(value = "pageNo") int pageNo,
			@RequestParam("searchName") String searchName,
			 ModelMap model) {
		int pageSize = 3;
		Page<Vocabulary> page = service.searchPageableVocabulary(pageNo, pageSize, searchName);
		List<Vocabulary> vocab = page.getContent();
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("itemStart", pageNo * pageSize);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("vocab", vocab);
		model.addAttribute("searchName", searchName);
		return "/user/vocabulary/home"; 
	}
}
