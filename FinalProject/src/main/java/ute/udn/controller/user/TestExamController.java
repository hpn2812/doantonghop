package ute.udn.controller.user;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ute.udn.entities.HistoryExamTest;
import ute.udn.entities.QuestionTestExamForm;
import ute.udn.entities.TestExam;
import ute.udn.entities.TestExamQuestion;
import ute.udn.entities.TestResult;
import ute.udn.entities.User;
import ute.udn.service.HistoryExamTestService;
import ute.udn.service.TestExamQuestionService;
import ute.udn.service.TestExamService;
import ute.udn.service.TestResultService;
import ute.udn.service.UserService;

@Controller
@RequestMapping("/user/test-exam")

public class TestExamController {

	@Autowired
	TestExamService testExamService;

	@Autowired
	TestResultService testResultService;

	@Autowired
	TestExamQuestionService testExamQuestionService;

	@Autowired
	UserService userService;

	@Autowired
	HistoryExamTestService historyExamTestService;
	

	@GetMapping("")
	private String tuvungpage(ModelMap model) {
		model.addAttribute("listQuesion", testExamService.findAll());
		return "/user/testexam/home";
	}

	// Phần nghe
	@GetMapping("/listen={testExamId}")
	public String questionListen(ModelMap model, @PathVariable("testExamId") Long testExamId, HttpSession session) {
		QuestionTestExamForm cdto = testExamQuestionService.getQuestions(testExamId);
		model.addAttribute("testExamId", testExamId);
		model.addAttribute("question", cdto);
		List<Integer> number = new ArrayList<>();
		for (int i = 1; i <= 50; ++i) {
			number.add(i);
		}
		model.addAttribute("number", number);
		session.setAttribute("startListenTime", new Date());
		return "/user/testexam/testListen";
	}

	@PostMapping("/submit")
	public String submitListen(
			ModelMap model,
			@ModelAttribute QuestionTestExamForm question,
			@RequestParam("testExamId") Long testExamId,
			HttpSession session,
			HttpServletRequest request) {
		
		try {
			session.setAttribute("results",testExamQuestionService.getResult(question));
			model.addAttribute("testExamId", testExamId);
			
			TestResult entity= new TestResult();
			entity.setTimeStartListeningTest((Date) request.getSession().getAttribute("startListenTime"));
			entity.setTimeSubmitListeningTest(new Date());
			entity.setTotalListenCorrect(testExamQuestionService.getResult(question));
			
			// add id ng dùng
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String username = principal.toString();

			if (principal instanceof UserDetails) {
				username = ((UserDetails) principal).getUsername();
			}
			User currentUser = userService.findByUsernameLike(username);
			entity.setUsers(currentUser);
			
			TestExam testExamEntity = new TestExam();
			testExamEntity.setTestExamId(testExamId);
			entity.setTestExam(testExamEntity);
			testResultService.save(entity);
			
			for(TestExamQuestion q: question.getQuestions()) {
				TestExamQuestion testExamQuestion = new TestExamQuestion();
				HistoryExamTest historyExamTest= new HistoryExamTest();
				testExamQuestion.setQuestionId(q.getQuestionId());
				historyExamTest.setAnwersOfUser(q.getChose());
				historyExamTest.setTestExamQuestion(testExamQuestion);
				historyExamTest.setTestResult(entity);
				historyExamTestService.save(historyExamTest);	
			}
			
			
			return "/user/testexam/resultListen";
		} catch (Exception e) {
			return "/user/testexam/resultListen";
		}

	}

	// Phần đọc
	@GetMapping("/reading={testExamId}")
	public String questionReading(ModelMap model, @PathVariable("testExamId") Long testExamId, HttpSession session) {
		try {
			QuestionTestExamForm cdto = testExamQuestionService.getQuestionReading(testExamId);
			model.addAttribute("question", cdto);
			List<Integer> number = new ArrayList<>();
			for (int i = 51; i <= 100; ++i) {
				number.add(i);
			}
			model.addAttribute("number", number);
			
			session.setAttribute("startReadingTime", new Date());
			return "/user/testexam/testReading";
		} catch (Exception e) {
			// TODO: handle exception
			return "/user/testexam/testReading";

		}

	}

	@PostMapping("/submitReading")
	public String submitReading(
			@ModelAttribute QuestionTestExamForm question,
			ModelMap model, 
			HttpSession session,
			HttpServletRequest request) {
		
		try {
			//kết quả bài thi
			int resultReading = testExamQuestionService.getResult(question);
			
			int resultListening = (Integer) request.getSession().getAttribute("results");

			int totalCorrect = resultReading + resultListening;
			model.addAttribute("result", resultReading);
			//current user
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String username = principal.toString();

			if (principal instanceof UserDetails) {
				username = ((UserDetails) principal).getUsername();
			}
			User currentUser = userService.findByUsernameLike(username);
			
			
			//get lastest id test result--> update it
			TestResult entity= testResultService.getTestResultByUserId(currentUser.getId());
			entity.setTimeStartReadingTest((Date) request.getSession().getAttribute("startReadingTime"));
			
			entity.setTimeSubmitReadingTest(new Date());
			
			entity.setTotalReadingCorrect(resultReading);
			
			entity.setTotalCorrect(totalCorrect);
			
			model.addAttribute("resultReading", resultReading);
			model.addAttribute("totalCorrect", totalCorrect);
			testResultService.save(entity);
			
			
			//lấy list câu hỏi của đề thi
			for(TestExamQuestion q: question.getQuestions()) {
				TestExamQuestion testExamQuestion = new TestExamQuestion();
				HistoryExamTest historyExamTest= new HistoryExamTest();
				testExamQuestion.setQuestionId(q.getQuestionId());
				historyExamTest.setAnwersOfUser(q.getChose());
				historyExamTest.setTestExamQuestion(testExamQuestion);
				historyExamTest.setTestResult(entity);
				historyExamTestService.save(historyExamTest);	
			}
			return "/user/testexam/resultReading";
		} catch (Exception e) {
			e.printStackTrace();
			return "/user/testexam/resultReading";
		}
		
	}


	@GetMapping("/page/{pageNo}")
	public String findPaginated(@PathVariable(value = "pageNo") int pageNo,
			@RequestParam("searchName") String searchName, ModelMap model) {
		int pageSize = 3;
		Page<TestExam> page = testExamService.searchPageableTestExam(pageNo, pageSize, searchName);
		List<TestExam> listQuesion = page.getContent();
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("itemStart", pageNo * pageSize);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("listQuesion", listQuesion);
		model.addAttribute("searchName", searchName);
		return "/user/testexam/home";
	}

}
