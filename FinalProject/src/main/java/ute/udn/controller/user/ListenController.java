package ute.udn.controller.user;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.input.WindowsLineEndingInputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ute.udn.entities.ContentListeningExcercise;
import ute.udn.entities.ListeningExcercise;
import ute.udn.entities.QuestionListenForm;
import ute.udn.entities.TestExamQuestion;
import ute.udn.service.ConListeningExcerciseService;
import ute.udn.service.ListeningExcerciseService;

@Controller
@RequestMapping("/user/listen")
public class ListenController {

	@Autowired
	ListeningExcerciseService service;

	@Autowired
	ConListeningExcerciseService conService;

	@GetMapping("")
	public String viewHomePage(ModelMap model) {
		return findPaginated(1, 0, 0, model);
	}

	@GetMapping("/{listenId}")
	public String infoListen(ModelMap model, @PathVariable("listenId") Long listenId) {
		QuestionListenForm cdto = conService.getQuestions(listenId);
		model.addAttribute("detail", service.getListeningExcersiceById(listenId));
		model.addAttribute("question", cdto);
		return "/user/listen/content";
	}

	@PostMapping("/submit")
	public String submit(
			@ModelAttribute QuestionListenForm question,
			HttpSession session,
			HttpServletRequest request,
			Model m) {
		m.addAttribute("result", conService.getResult(question));
		List<ContentListeningExcercise> list= new ArrayList<ContentListeningExcercise>();
		
		
		for(ContentListeningExcercise q: question.getQuestions()) {
			System.out.println(q.getCListentId());
			System.out.println(q.getQuestion());
			System.out.println(q.getOption1());
			
//			ContentListeningExcercise con= new ContentListeningExcercise();
//			con.setCListentId(q.getCListentId());
//			con.setQuestion(q.getQuestion());
//			con.setCorrectOption(q.getCorrectOption());
//			con.setOption1(q.getOption1());
//			con.setOption2(q.getOption2());
//			con.setOption3(q.getOption3());
//			con.setOption4(q.getOption4());
//			con.setExplains(q.getExplains());
//			con.setImage(q.getImage());
//			con.setChose(q.getChose());
//			list.add(con);
		}
		
//		for (ContentListeningExcercise con : list) {
//			System.out.println(con.getAudio());
//			System.out.println(con.getOption2());
//			System.out.println(con.getCorrectOption());
//			
//		}
		
		session.setAttribute("listAnserListen", list);
		return "/user/listen/result";
	}
	
	@GetMapping("/answer")
	public String getAnswerOfListen(ModelMap model,HttpSession session,
			HttpServletRequest request) {
		return "/user/listen/infoDoExam";
	}
	
	

	// phân trang
	@GetMapping("/page/{pageNo}")
	public String findPaginated(@PathVariable(value = "pageNo") int pageNo,
			@RequestParam("searchPart") int searchPart,
			@RequestParam("searchLevel") int searchLevel,
			ModelMap model) {
		int pageSize = 3;
		Page<ListeningExcercise> page = service.searchPageableUser(pageNo, pageSize,searchPart,searchLevel);
		List<ListeningExcercise> listen = page.getContent();
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("itemStart", pageNo * pageSize);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("listListening", listen);
		model.addAttribute("searchPart", searchPart);
		model.addAttribute("searchLevel", searchLevel);
		return "/user/listen/home";
	}

}
