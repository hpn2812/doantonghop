package ute.udn.controller.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ute.udn.entities.ListeningExcercise;
import ute.udn.entities.QuestionListenForm;
import ute.udn.entities.QuestionReadingForm;
import ute.udn.entities.ReadingExcercise;
import ute.udn.service.ConReadingExcerciseService;
import ute.udn.service.ReadingExcerciseService;

@Controller
@RequestMapping("/user/reading")
public class ReadController {
	
	@Autowired
	ReadingExcerciseService readingService;
	
	@Autowired
	ConReadingExcerciseService conReadingSerice;
	
	@GetMapping("")
	public String viewHomePage(ModelMap model) {
		return findPaginated(1, 0, 0, model);
	}
	
	@GetMapping("/{readId}")
	public String infoListen(ModelMap model, @PathVariable("readId") Long readId) {
	
			QuestionReadingForm cdto = conReadingSerice.getQuestions(readId);
			model.addAttribute("detail", readingService.getReadingExcersiceById(readId));
			model.addAttribute("question", cdto);
			return "/user/reading/content";
	
	}
	
	@GetMapping("/page/{pageNo}")
	public String findPaginated(@PathVariable(value = "pageNo") int pageNo,
			@RequestParam("searchPart") int searchPart,
			@RequestParam("searchLevel") int searchLevel,
			ModelMap model) {
			int pageSize = 3;
			Page<ReadingExcercise> page = readingService.searchPageableUser(pageNo, pageSize, searchPart,searchLevel);
			List<ReadingExcercise> list = page.getContent();
			model.addAttribute("currentPage", pageNo);
			model.addAttribute("pageSize", pageSize);
			model.addAttribute("itemStart", pageNo * pageSize);
			model.addAttribute("totalPages", page.getTotalPages());
			model.addAttribute("totalItems", page.getTotalElements());
			model.addAttribute("list", list);
			model.addAttribute("searchPart", searchPart);
			model.addAttribute("searchLevel", searchLevel);
			return "/user/reading/home";
	}
	
	
	@PostMapping("/submit")
	public String submit(@ModelAttribute QuestionReadingForm question, Model m) {
		m.addAttribute("result", conReadingSerice.getResult(question));
		return "/user/reading/result";
	}
	


}
