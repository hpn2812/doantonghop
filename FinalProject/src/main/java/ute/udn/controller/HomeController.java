package ute.udn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import ute.udn.entities.Vocabulary;
import ute.udn.service.InfomationService;
import ute.udn.service.VocabularyService;

@Controller
public class HomeController {
	
	
	@Autowired
	InfomationService infoService;
	
	@GetMapping("")
	private String homepage() {
		return "/user/home"; 
	}
	
	

}
