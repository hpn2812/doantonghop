package ute.udn.controller.teacher;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFShape;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ute.udn.entities.ContentListeningExcercise;
import ute.udn.entities.ContentReadingExcercise;
import ute.udn.entities.ContentVocabulary;
import ute.udn.entities.ListeningExcercise;
import ute.udn.entities.ReadingExcercise;
import ute.udn.entities.User;
import ute.udn.model.ConListeningDto;
import ute.udn.model.ConReadingDto;
import ute.udn.model.ReadingExcerciseDto;
import ute.udn.service.ConReadingExcerciseService;
import ute.udn.service.ReadingExcerciseService;
import ute.udn.service.UserService;
import ute.udn.util.VocabularyExporter;
import ute.udn.util.ReadingExporter;
import ute.udn.util.SaveFile;

@Controller
@RequestMapping("/teacher/reading")
public class ReadingController {
	@Autowired
	ReadingExcerciseService service;

	@Autowired
	ConReadingExcerciseService conReadingService;
	
	@Autowired
	UserService userService;

	@GetMapping("")
	public String viewHomePage(ModelMap model) {
		return readingFindPaginated(1, "readId", "asc", 0, 0, model);
	}

	@GetMapping("/add")
	public String add(ModelMap model) {
		model.addAttribute("reading", new ReadingExcerciseDto());
		return "/teacher/reading/saveReading";
	}

	@PostMapping("/save")
	public String saveReading(ModelMap model, @Valid @ModelAttribute("reading") ReadingExcerciseDto dto,
			BindingResult result, @RequestParam("file_imageQuestion") MultipartFile file,
			@RequestParam("file_Excel") MultipartFile fileExcel){

		try {
			
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String username = principal.toString();

			if (principal instanceof UserDetails) {
				username = ((UserDetails) principal).getUsername();
			}
			User currentUser = userService.findByUsernameLike(username);
			// upload file image
			UUID uuid = UUID.randomUUID();
			String fileName = uuid.toString() + StringUtils.cleanPath(file.getOriginalFilename());
			String uploadDirImage = "src\\main\\resources\\static\\resources\\file\\images\\baidoc\\";
			SaveFile.saveFile(uploadDirImage, fileName, file);
			String pathFile = "/resources/file/images/baidoc/" + fileName;
			dto.setImage(pathFile);
			// save ReadingExcercise
			ReadingExcercise entity = new ReadingExcercise();
			BeanUtils.copyProperties(dto, entity);
			entity.setUsers(currentUser);
			service.save(entity);

			Workbook workbook = new XSSFWorkbook(fileExcel.getInputStream());
			Sheet datatypeSheet = workbook.getSheetAt(0);
			DataFormatter fmt = new DataFormatter();
			Iterator<Row> iterator = datatypeSheet.iterator();
			while(iterator.hasNext()) {
				Row currentRow = iterator.next();
				ContentReadingExcercise conReading = new ContentReadingExcercise();
				conReading.setParagrahps(fmt.formatCellValue(currentRow.getCell(0)));
				conReading.setQuestion(fmt.formatCellValue(currentRow.getCell(1)));
				conReading.setA(fmt.formatCellValue(currentRow.getCell(2)));
				conReading.setB(fmt.formatCellValue(currentRow.getCell(3)));
				conReading.setC(fmt.formatCellValue(currentRow.getCell(4)));
				conReading.setD(fmt.formatCellValue(currentRow.getCell(5)));
				conReading.setCorrectReading(fmt.formatCellValue(currentRow.getCell(6)));
				conReading.setExplains(fmt.formatCellValue(currentRow.getCell(7)));
				conReading.setChose("e");
				conReading.setReadingExcercise(entity);
				conReadingService.save(conReading);
			}
			workbook.close();
			model.addAttribute("message", "Thêm bài đọc mới thành công!");
			return readingFindPaginated(1, "readId", "asc", 0, 0, model);
			
		} catch (Exception e) {
			model.addAttribute("error", "Thêm bài đọc mới thất bại!");
			return readingFindPaginated(1, "readId", "asc", 0, 0, model);
		}
		

	}

	@PostMapping("/update")
	public String updateReading(ModelMap model, @Valid @ModelAttribute("reading") ReadingExcerciseDto dto,
			@RequestParam("file_imageQuestion") MultipartFile file, BindingResult result) throws IOException {
		try {
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String username = principal.toString();

			if (principal instanceof UserDetails) {
				username = ((UserDetails) principal).getUsername();
			}
			User currentUser = userService.findByUsernameLike(username);
			ReadingExcercise entity = new ReadingExcercise();
			if (file.isEmpty()) {
				entity.setImage(dto.getImage());
			} else {
				String fileName = StringUtils.cleanPath(file.getOriginalFilename());
				String uploadDirImage = "src\\main\\resources\\static\\resources\\file\\images\\baidoc\\";
				SaveFile.saveFile(uploadDirImage, fileName, file);
				String pathFile = "/resources/file/images/baidoc/" + fileName;
				dto.setImage(pathFile);
			}

			BeanUtils.copyProperties(dto, entity);
			// save ReadingExcercise
			entity.setUsers(currentUser);
			service.save(entity);
			model.addAttribute("message", "Cập nhật bài đọc thành công!");
			return readingFindPaginated(1, "readId", "asc", 0, 0, model);

		} catch (Exception e) {
			// TODO: handle exception
			model.addAttribute("error", "Cập nhật bài đọc thất bại!" + e);
			return readingFindPaginated(1, "readId", "asc", 0, 0, model);
		}

	}

	@GetMapping("/edit-id={readId}")
	public String edit(ModelMap model, @PathVariable("readId") Long readId) {
		// set thành chế độ edit
		ReadingExcerciseDto dto = new ReadingExcerciseDto();
		dto.setIsEdit(true);
		ReadingExcercise entity = service.getReadingExcersiceById(readId);
		BeanUtils.copyProperties(entity, dto);
		model.addAttribute("reading", dto);
		return "/teacher/reading/updateReading";
	}

	@GetMapping("/delete-id={readId}")
	public String delete(ModelMap model, @PathVariable("readId") Long readId) {
		try {
			service.deleteById(readId);
			model.addAttribute("message", "Xóa bài đọc thành công!");

			return readingFindPaginated(1, "readId", "asc", 0, 0, model);
		} catch (Exception e) {

			model.addAttribute("error", "Bài đọc không tồn tại trên hệ thống!");
			return readingFindPaginated(1, "readId", "asc", 0, 0, model);
		}

	}

	@GetMapping("/page/{pageNo}")
	public String readingFindPaginated(
			@PathVariable(value = "pageNo") int pageNo,
			@RequestParam("sortField") String sortField, 
			@RequestParam("sortDir") String sortDir,
			@RequestParam("searchPart") int searchPart,
			@RequestParam("searchLevel") int searchLevel, 
			ModelMap model) {

		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = principal.toString();

		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		}
		User currentUser = userService.findByUsernameLike(username);
		
		int pageSize = 5;
		Page<ReadingExcercise> page = service.searchPageableByUserId(currentUser.getId(),pageNo, pageSize, sortField, sortDir,searchLevel,searchPart);
		List<ReadingExcercise> reading = page.getContent();
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("itemStart", pageNo * pageSize);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
		model.addAttribute("reading", reading);
		model.addAttribute("searchPart", searchPart);
		model.addAttribute("searchLevel", searchLevel);
		return "/teacher/reading/list";
	}

	/*
	 * ================================================Content
	 * Reading===========================================================
	 */

	@GetMapping("/info-id={readId}/page/{pageNo}")
	public String findPaginated(@PathVariable(value = "readId") long readId, @PathVariable(value = "pageNo") int pageNo,
			ModelMap model) {
		
		int pageSize = 5;
		Page<ContentReadingExcercise> page = conReadingService.pageable(readId, pageNo, pageSize);
		List<ContentReadingExcercise> readingDetail = page.getContent();
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("itemStart", pageNo * pageSize);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("readId", readId);
		//model.addAttribute("name", service.findByName(readId));
		model.addAttribute("readingDetail", readingDetail);
		return "/teacher/reading/listQuestion";
	}

	@GetMapping("/delete-question-id={crId}/{id}")
	public String deleteQuestion(ModelMap model, @PathVariable("crId") Long crId, @PathVariable("id") Long id) {
		try {
			conReadingService.deleteById(crId);
			model.addAttribute("message", "Xóa câu hỏi bài đọc thành công!");
			return findPaginated(id, 1, model);
		} catch (Exception e) {
			model.addAttribute("error", "Câu hỏi bài đọc không tồn tại trên hệ thống!");
			return findPaginated(id, 1, model);
		}

	}

	@GetMapping("/add-reading-question={readId}")
	public String addQuestion(ModelMap model, @PathVariable("readId") Long id) {
		model.addAttribute("readingId", id);
		model.addAttribute("readingQuestion", new ConReadingDto());
		return "/teacher/reading/saveOrEditQuestion";
	}

	@PostMapping("/save-question")
	public String saveOrUpdateQuestion(ModelMap model, @Valid @ModelAttribute("readingQuestion") ConReadingDto dto,
			BindingResult result, @RequestParam("readingId") Long id) {

		try {
			if (result.hasErrors()) {
				model.addAttribute("readingId", id);
				return "/teacher/reading/saveOrEditQuestion";
			}
			ReadingExcercise ex = new ReadingExcercise();
			ex.setReadId(id);
			dto.setReadingExcercise(ex);
			ContentReadingExcercise entity = new ContentReadingExcercise();
			// kiểm tra ở chế độ edit đã tồn tại file chưa
			BeanUtils.copyProperties(dto, entity);
			// save ListeningExcercise
			conReadingService.save(entity);

			if (dto.getIsEdit() == true) {
				model.addAttribute("message", "Cập nhật câu hỏi bài đọc thành công");
			}
			if (dto.getIsEdit() == false) {
				model.addAttribute("message", "Thêm mới câu hỏi bài đọc thành công");
			}

			return findPaginated(id, 1, model);

		} catch (Exception e) {
			if (dto.getIsEdit() == true) {
				model.addAttribute("error", "Cập nhật câu hỏi bài đọc thất bại");
			}
			if (dto.getIsEdit() == false) {
				model.addAttribute("error", "Thêm mới câu hỏi bài đọc thất bại");
			}
			return findPaginated(id, 1, model);
		}

	}

	@GetMapping("/edit-question-id={crId}/{id}")
	public String editReadingQuestion(ModelMap model, @PathVariable("crId") Long crId, @PathVariable("id") Long id) {
		ConReadingDto dto = new ConReadingDto();
		dto.setIsEdit(true);
		model.addAttribute("readingId", id);

		ContentReadingExcercise entity = conReadingService.getReadingQuestionById(crId);
		BeanUtils.copyProperties(entity, dto);

		model.addAttribute("readingQuestion", dto);
		return "/teacher/reading/saveOrEditQuestion";
	}

	@GetMapping("/export/excel")
	public void exportToExcel(HttpServletResponse response) throws IOException {

		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=reading_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<ContentReadingExcercise> listVocab = conReadingService.findAll();

		ReadingExporter excelExporter = new ReadingExporter(listVocab);

		excelExporter.export(response);
	}

}
