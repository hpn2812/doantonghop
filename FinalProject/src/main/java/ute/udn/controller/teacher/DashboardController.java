package ute.udn.controller.teacher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ute.udn.entities.TestExam;
import ute.udn.entities.User;
import ute.udn.service.GrammarService;
import ute.udn.service.ListeningExcerciseService;
import ute.udn.service.ReadingExcerciseService;
import ute.udn.service.TestExamService;
import ute.udn.service.TestResultService;
import ute.udn.service.UserService;
import ute.udn.service.VocabularyService;

@Controller
@RequestMapping("/teacher")
public class DashboardController {
	
	@Autowired
	GrammarService grammarService;
	
	@Autowired
	ListeningExcerciseService listenExcerciseService;
	
	@Autowired
	ReadingExcerciseService readingExcerciseService;
	
	@Autowired
	TestExamService testExamService;
	
	@Autowired
	VocabularyService vocabularyService;
	
	@Autowired	
	UserService userService;
	
	@Autowired
	TestResultService testResultService;
	
	@RequestMapping(value = { "/", "" }, method = RequestMethod.GET)
	public String dashboarPage(ModelMap model) {
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = principal.toString();

		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		}
		User currentUser = userService.findByUsernameLike(username);
		
		model.addAttribute("countGramamr", grammarService.countGrammarByUserId(currentUser.getId()));
		
		model.addAttribute("countListen", listenExcerciseService.countListenExcerciseByUserId(currentUser.getId()));
		
		model.addAttribute("countReading", readingExcerciseService.countReadingExcerciseByUserId(currentUser.getId()));
		
		model.addAttribute("countTestExam", testExamService.countTestExamByUserId(currentUser.getId()));
		
		model.addAttribute("countVocabulary", vocabularyService.countVocabularyByUserId(currentUser.getId()));
		
		model.addAttribute("countTestResult", testResultService.countTestResultOfWeek(currentUser.getId()));
		
		return "/teacher/dashboard/home";
	}
	
}
