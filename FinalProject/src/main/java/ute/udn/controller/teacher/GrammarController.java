package ute.udn.controller.teacher;

import java.util.List;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;


import ute.udn.entities.Grammar;
import ute.udn.entities.User;
import ute.udn.model.GrammarDto;
import ute.udn.service.GrammarService;
import ute.udn.service.UserService;
import ute.udn.util.SaveFile;

@Controller
@RequestMapping("/teacher/grammar")
public class GrammarController {

	@Autowired
	GrammarService service;
	
	@Autowired
	UserService userService;

	@GetMapping("")
	public String viewHomePage(ModelMap model) {
		return findPaginatedByUserId(1,"grammarId", "asc", "",model);
	}

	@GetMapping("/add")
	public String add(ModelMap model) {
		model.addAttribute("grammars", new GrammarDto());
		return "/teacher/grammar/saveOrEditGrammar";
	}

	@PostMapping("/saveOrUpdate")
	public String saveOrUpdate(ModelMap model, @Valid @ModelAttribute("grammars") GrammarDto dto, BindingResult result,
			@RequestParam("file_imageQuestion") MultipartFile file) {
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = principal.toString();

		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		}
		User currentUser = userService.findByUsernameLike(username);
		
		try {
			Grammar entity = new Grammar();
			if (result.hasErrors()) {
				return "/teacher/grammar/saveOrEditGrammar";
			}
			UUID uuid = UUID.randomUUID();
			String file_Image = uuid.toString() + StringUtils.cleanPath(file.getOriginalFilename());
			String uploadDirImage = "src\\main\\resources\\static\\resources\\file\\images\\nguphap\\";// dir
			String fullPath = "/resources/file/images/nguphap/" + file_Image; // save path image in db
			SaveFile.saveFile(uploadDirImage, file_Image, file);
			
			if(file.isEmpty()) {
				entity.setImage(dto.getImage());
			}else {
				dto.setImage(fullPath);
			}
			
			BeanUtils.copyProperties(dto, entity);
			entity.setUsers(currentUser);
			service.save(entity);
			if(dto.getIsEdit()==true) {
				model.addAttribute("message", "Cập nhật thành công!");
			}
			else {
				model.addAttribute("message", "Thêm mới thành công!");
			}
			
			return findPaginatedByUserId(1,"grammarId", "asc", "",model);
		} catch (Exception e) {
			model.addAttribute("error", "Thêm mới thất bại!");
			return findPaginatedByUserId(1,"grammarId", "asc", "",model);
		}
		

	}

	@GetMapping("/edit-id={grammarId}")
	public String editListen(ModelMap model, @PathVariable("grammarId") Long grammarId) {

		GrammarDto dto = new GrammarDto();
		dto.setIsEdit(true);
		Grammar entity = service.getGrammarById(grammarId);
		BeanUtils.copyProperties(entity, dto);
		model.addAttribute("grammars", dto);
		return "/teacher/grammar/saveOrEditGrammar";
	}

	@GetMapping("/delete-id={grammarId}")
	public String delete(ModelMap model, @PathVariable("grammarId") Long grammarId) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = principal.toString();

		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		}
		User currentUser = userService.findByUsernameLike(username);
		try {
			service.deleteById(grammarId);
			model.addAttribute("message", "Xóa thành công!");
			return findPaginatedByUserId(1,"grammarId", "asc", "",model);
		} catch (Exception e) {
			model.addAttribute("error", "Xóa thất bại!");
			return findPaginatedByUserId(1,"grammarId", "asc", "",model);
		}
		
	}
	
	@GetMapping("/page/{pageNo}")
	public String findPaginatedByUserId(
			@PathVariable(value = "pageNo") int pageNo, 
			@RequestParam("sortField") String sortField,
			@RequestParam("sortDir") String sortDir,
			@RequestParam("searchName") String searchName,
			ModelMap model) {
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = principal.toString();

		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		}
		User currentUser = userService.findByUsernameLike(username);
		
		int pageSize = 5;
		Page<Grammar> page = service.searchPageableByUserId(currentUser.getId(),pageNo, pageSize, sortField, sortDir,searchName);
		List<Grammar> grammars = page.getContent();
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("itemStart", pageNo * pageSize);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
		model.addAttribute("grammars", grammars);
		model.addAttribute("searchName", searchName);
		model.addAttribute("id", currentUser.getId());
		return "/teacher/grammar/listGrammar";
	}

}
