package ute.udn.controller.teacher;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import java.util.UUID;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFShape;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import ute.udn.entities.ContentListeningExcercise;
import ute.udn.entities.ContentVocabulary;
import ute.udn.entities.ListeningExcercise;
import ute.udn.entities.User;
import ute.udn.model.ConListeningDto;
import ute.udn.model.ListeningExcerciseDto;
import ute.udn.service.ConListeningExcerciseService;
import ute.udn.service.ListeningExcerciseService;
import ute.udn.service.UserService;
import ute.udn.util.VocabularyExporter;
import ute.udn.util.ListeningExporter;
import ute.udn.util.SaveFile;

@Controller
@RequestMapping("/teacher/listen")
public class ListeningController {

	@Autowired
	ListeningExcerciseService listeningService;

	@Autowired
	ConListeningExcerciseService listeningQuestionService;
	
	@Autowired
	UserService userService;

//	================Listen======================

	@RequestMapping(value = { "/", "" }, method = RequestMethod.GET)
	public String viewHomePage(ModelMap model) {
		return findPaginated(1, "listenId", "asc", 0, 0, model);
	}

	@GetMapping("/add-listening")
	public String add(ModelMap model) {
		model.addAttribute("listen", new ListeningExcerciseDto());
		return "/teacher/listen/saveListen";
	}

	// save Listening
	@PostMapping("/listen-save")
	public String saveOrUpdate(
			ModelMap model, 
			@Valid @ModelAttribute("listen") ListeningExcerciseDto dto,
			BindingResult result, @RequestParam("file_imageQuestion") MultipartFile file,
			@RequestParam("file_Excel") MultipartFile fileExcel, 
			@RequestParam("file_listen") MultipartFile fileAudio) {
		try {
			if (result.hasErrors()) {
				return "/teacher/listen/saveListen";
			} else {
				
				Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				String username = principal.toString();

				if (principal instanceof UserDetails) {
					username = ((UserDetails) principal).getUsername();
				}
				User currentUser = userService.findByUsernameLike(username);
				UUID uuid = UUID.randomUUID();
				// save file image
				String file_Image = uuid.toString() + StringUtils.cleanPath(file.getOriginalFilename());																			// image
				String uploadDirImage = "src\\main\\resources\\static\\resources\\file\\images\\bainghe\\";
				String fullPath = "/resources/file/images/bainghe/" + file_Image; 
				SaveFile.saveFile(uploadDirImage, file_Image, file);
				if (file_Image.isEmpty()) {
					dto.setImage("");
				}else {
					dto.setImage(fullPath);
				}
				
				// save listenExcerise
				ListeningExcercise entity = new ListeningExcercise();
				BeanUtils.copyProperties(dto, entity);
				entity.setUsers(currentUser);
				listeningService.save(entity);
				// save file audio
				String file_Audio = uuid.toString() + StringUtils.cleanPath(fileAudio.getOriginalFilename());
				String uploadDirAudio = "src\\main\\resources\\static\\resources\\file\\audio\\bainghe\\";
				String fullPathAudio = "/resources/file/audio/bainghe/" + file_Audio;
				SaveFile.savePathFile(fileAudio, file_Audio, uploadDirAudio);
				
				// read file excel
				List<ContentListeningExcercise> listCauHoi = new ArrayList<>();
				Workbook workbook = new XSSFWorkbook(fileExcel.getInputStream());
				Sheet datatypeSheet = workbook.getSheetAt(0);
				DataFormatter fmt = new DataFormatter();
				Iterator<Row> iterator = datatypeSheet.iterator();
				XSSFDrawing dp = (XSSFDrawing) datatypeSheet.createDrawingPatriarch();
				List<XSSFShape> pics = dp.getShapes();
				
				while (iterator.hasNext()) {
					Row currentRow = iterator.next();
					ContentListeningExcercise listeningExcercise = new ContentListeningExcercise();
					// save name of audio
					listeningExcercise.setAudio(fullPathAudio);
					if (fmt.formatCellValue(currentRow.getCell(0)) == null) {
						listeningExcercise.setQuestion(" ");
					}
					listeningExcercise.setQuestion(fmt.formatCellValue(currentRow.getCell(0)));
					listeningExcercise.setOption1(fmt.formatCellValue(currentRow.getCell(1)));
					listeningExcercise.setOption2(fmt.formatCellValue(currentRow.getCell(2)));
					listeningExcercise.setOption3(fmt.formatCellValue(currentRow.getCell(3)));
					listeningExcercise.setOption4(fmt.formatCellValue(currentRow.getCell(4)));
					listeningExcercise.setCorrectOption(fmt.formatCellValue(currentRow.getCell(5)));
					listeningExcercise.setExplains(fmt.formatCellValue(currentRow.getCell(6)));
					// read image in file excel
					for (Iterator<? extends XSSFShape> it = pics.iterator(); it.hasNext();) {
						XSSFPicture inpPic = (XSSFPicture) it.next();
						XSSFClientAnchor clientAnchor = inpPic.getClientAnchor();
						PictureData pict = inpPic.getPictureData();
						byte[] data = pict.getData();

						String uuString = "image_" + uuid.toString();

						if (clientAnchor.getCol1() == 7 && clientAnchor.getRow1() == currentRow.getRowNum()) {
							byte[] imageData = data;

							String pathImageInExcel = "src\\main\\resources\\static\\resources\\file\\images\\bainghe\\cauhoibainghe\\";
							FileOutputStream out = new FileOutputStream(pathImageInExcel + uuString + ".png");
							String fullPathImageQuestion = "/resources/file/images/bainghe/cauhoibainghe/" + uuString
									+ ".png";
							out.write(imageData);
							out.close();
							listeningExcercise.setImage(fullPathImageQuestion);
						}
					}
					listeningExcercise.setChose("e");
					listeningExcercise.setExcercise(entity);
					listCauHoi.add(listeningExcercise);
					
					listeningQuestionService.save(listeningExcercise);
				}
				workbook.close();
				model.addAttribute("message", "Thêm mới đề luyện nghe thành công!");
				return findPaginated(1, "listenId", "asc", 0, 0, model);

			}

		} catch (Exception e) {
			
			model.addAttribute("message", "Thêm mới đề luyện nghe thành công!");
			return findPaginated(1, "listenId", "asc", 0, 0, model);
		}

	}

	// edit listening page
	@GetMapping("/edit-id={listenId}")
	public String editListen(ModelMap model, @PathVariable("listenId") Long listenId) {
		ListeningExcerciseDto dto = new ListeningExcerciseDto();
		ListeningExcercise entity = listeningService.getListeningExcersiceById(listenId);
		BeanUtils.copyProperties(entity, dto);
		model.addAttribute("id", listenId);
		model.addAttribute("listen", dto);
		return "/teacher/listen/updateListening";
	}

	// update Listening
	@PostMapping("/update-listening")
	public String update(ModelMap model,
			@Valid @ModelAttribute("listen") ListeningExcerciseDto dto,
			BindingResult result,
			@RequestParam("file_imageQuestion") MultipartFile file
			) {
		try {
			
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String username = principal.toString();

			if (principal instanceof UserDetails) {
				username = ((UserDetails) principal).getUsername();
			}
			User currentUser = userService.findByUsernameLike(username);
			if (result.hasErrors()) {
				model.addAttribute("id", dto.getListenId());
				return "/teacher/listen/updateListening";
			}else {
				ListeningExcercise entity = new ListeningExcercise();
				UUID uuid = UUID.randomUUID();
				String file_Image = uuid + StringUtils.cleanPath(file.getOriginalFilename());
				if (file.isEmpty()) {
					entity.setImage(dto.getImage());
				} else {
					// upload file image

					file_Image = uuid.toString() + StringUtils.cleanPath(file.getOriginalFilename());// change name image

					String uploadDirImage = "src\\main\\resources\\static\\resources\\file\\images\\bainghe\\";// dir

					String fullPath = "/resources/file/images/bainghe/" + file_Image; // save path image in db

					SaveFile.saveFile(uploadDirImage, file_Image, file); // save file to source
					dto.setImage(fullPath);
				}
				BeanUtils.copyProperties(dto, entity);
				entity.setUsers(currentUser);
				listeningService.save(entity);
				model.addAttribute("message", "Cập nhật đề luyện nghe thành công!");
				return findPaginated(1, "listenId", "asc", 0, 0, model);
			}
		} catch (IOException e) {
			model.addAttribute("error", "Cập nhật đề luyện nghe thất bại!");
			return findPaginated(1, "listenId", "asc", 0, 0, model);
		}

	}
	
	
	

	// delete listening
	@GetMapping("/delete-id={listenId}")
	public String delete(ModelMap model, @PathVariable("listenId") Long listenId) {
		try {

			listeningService.deleteById(listenId);
			model.addAttribute("message", "Xóa đề luyện nghe thành công");
			return findPaginated(1, "listenId", "asc", 0, 0, model);

		} catch (Exception e) {
			model.addAttribute("error", "Đề luyện nghe không tồn tại trên hệ thống");
			return findPaginated(1, "listenId", "asc", 0, 0, model);
		}

	}

	// phân trang
	@GetMapping("/page/{pageNo}")
	public String findPaginated(
			@PathVariable(value = "pageNo") int pageNo, 
			@RequestParam("sortField") String sortField,
			@RequestParam("sortDir") String sortDir, 
			@RequestParam("searchLevel") int searchLevel, 
			@RequestParam("searchPart") int searchPart,
			ModelMap model) {
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = principal.toString();

		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		}
		User currentUser = userService.findByUsernameLike(username);
		int pageSize = 5;
		
		
		Page<ListeningExcercise> page = listeningService.searchPageableByUserId(currentUser.getId(),pageNo, pageSize, sortField, sortDir,searchLevel,searchPart);
		List<ListeningExcercise> listen = page.getContent();
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("itemStart", pageNo * pageSize);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
		model.addAttribute("listListening", listen);
		model.addAttribute("searchPart", searchPart);
		model.addAttribute("searchLevel", searchLevel);
		model.addAttribute("id", currentUser.getId());
		
		return "/teacher/listen/list";
	}

//	=============================================== Listen Question================================================

	@GetMapping("/info-id={cListentId}/page/{pageNo}")
	public String findPaginated(@PathVariable(value = "cListentId") long cListentId,
			@PathVariable(value = "pageNo") int pageNo, ModelMap model) {
		int pageSize = 5;
		Page<ContentListeningExcercise> page = listeningQuestionService.pageable(cListentId, pageNo, pageSize);
		List<ContentListeningExcercise> listenDetail = page.getContent();
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("itemStart", pageNo * pageSize);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("cListentId", cListentId);
		model.addAttribute("listenDetail", listenDetail);
		model.addAttribute("detail",listeningService.getListeningExcersiceById(cListentId));
		
		return "/teacher/listen/listQuestion";
	}

	@GetMapping("/delete-question-id={cListentId}/{id}")
	public String deleteQuestion(ModelMap model, @PathVariable("cListentId") Long cListentId,
			@PathVariable("id") Long ids) {
		try {
			listeningQuestionService.deleteById(cListentId);
			model.addAttribute("message", "Xóa câu hỏi thành công!");
			return findPaginated(ids, 1, model);

		} catch (Exception e) {
			model.addAttribute("error", "Câu hỏi không tồn tại trên hệ thống");
			return findPaginated(ids, 1, model);
		}

	}

	@GetMapping("/add-listening-question={id}")
	public String addQuestion(ModelMap model, @PathVariable("id") Long ids) {
		model.addAttribute("idQuestion", ids);
		model.addAttribute("listenQuestion", new ConListeningDto());
		return "/teacher/listen/saveOrEidtQuestion";
	}

	// save Listening Question
	@PostMapping("/listen-save-question")
	public String saveOrUpdateQuestion(ModelMap model, 
			@Valid @ModelAttribute("listenQuestion") ConListeningDto dto,BindingResult result,
			@RequestParam("file_imageQuestion") MultipartFile file,
			@RequestParam("file_listen") MultipartFile fileAudio,
			@RequestParam("listenId") Long id) {
		try {
			// error --> validation
			if (result.hasErrors()) {
				model.addAttribute("idQuestion", id);
				return "/teacher/listen/saveOrEidtQuestion";
			}
			ListeningExcercise listenEnity = new ListeningExcercise();
			ContentListeningExcercise questionListenEntity = new ContentListeningExcercise();
			UUID uuid = UUID.randomUUID();
			// upload file image
			if (file.isEmpty()) {
				questionListenEntity.setImage(dto.getImage());
			} else {
				String file_Image = uuid.toString() + StringUtils.cleanPath(file.getOriginalFilename());// change name
				String uploadDirImage = "src\\main\\resources\\static\\resources\\file\\images\\bainghe\\cauhoibainghe\\";
				String fullPath = "/resources/file/images/bainghe/cauhoibainghe/" + file_Image; // save path image in db
				SaveFile.saveFile(uploadDirImage, file_Image, file);
				dto.setImage(fullPath);

			}
			// nếu là chế độ edit check ng dùng có nhập file không --> nếu ko nhập file thì
			// set path audio cũ
			if (fileAudio.isEmpty()) {
				questionListenEntity.setAudio(dto.getAudio());
			} else {
				String file_Audio = uuid.toString() + StringUtils.cleanPath(fileAudio.getOriginalFilename());// change
																												// name
				String uploadDirAudio = "src\\main\\resources\\static\\resources\\file\\audio\\bainghe\\";
				String fullPathAudio = "/resources/file/audio/bainghe/" + file_Audio; // save path to db
				SaveFile.savePathFile(fileAudio, file_Audio, uploadDirAudio);
				dto.setAudio(fullPathAudio);
			}
			listenEnity.setListenId(id);
			dto.setExcercise(listenEnity);
			BeanUtils.copyProperties(dto, questionListenEntity);
			// save ListeningExcercise
			listeningQuestionService.save(questionListenEntity);
			if (dto.getIsEdit() == false) {
				model.addAttribute("message", "Thêm mới câu hỏi cho đề luyện nghe thành công");
			} else {
				model.addAttribute("message", "Cập nhật câu hỏi cho đề luyện nghe thành công");
			}

			return findPaginated(id, 1, model);
		} catch (Exception e) {
			if (dto.getIsEdit() == false) {
				model.addAttribute("error", "Thêm mới câu hỏi cho đề luyện nghe thất bại");
			} else {
				model.addAttribute("error", "Cập nhật câu hỏi cho đề luyện nghe thất bại");
			}
			
			return findPaginated(id, 1, model);
		}

	}

	@GetMapping("/edit-question-id={cListentId}/{id}")
	public String editListenQuestion(ModelMap model, @PathVariable("cListentId") Long listenId,
			@PathVariable("id") Long ids) {
		ConListeningDto dto = new ConListeningDto();
		dto.setIsEdit(true);
		model.addAttribute("idQuestion", ids);

		ContentListeningExcercise entity = listeningQuestionService.getListeningQuestionById(listenId);
		BeanUtils.copyProperties(entity, dto);

		model.addAttribute("listenQuestion", dto);
		return "/teacher/listen/saveOrEidtQuestion";
	}

	@GetMapping("/export/excel/{listen_id}")
	public void exportToExcel(@PathVariable("listen_id") Long listen_id, HttpServletResponse response)
			throws IOException {

		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=Lisen_ID_"+listen_id+"_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<ContentListeningExcercise> listListen = listeningQuestionService
				.getListListenQuestionByListenId(listen_id);

		ListeningExporter excelExporter = new ListeningExporter(listListen);
		excelExporter.export(response);
	}

}
