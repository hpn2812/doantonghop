package ute.udn.controller.teacher;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ute.udn.entities.TestResult;
import ute.udn.entities.User;
import ute.udn.service.HistoryExamTestService;
import ute.udn.service.TestResultService;
import ute.udn.service.UserService;

@Controller
@RequestMapping("/teacher/history")
public class HistoryTestResultController {

	@Autowired
	TestResultService testResultService;
	
	@Autowired
	HistoryExamTestService historyExamTestService;
	@Autowired
	UserService userService;
	
	@RequestMapping(value = { "/", "" }, method = RequestMethod.GET)
	public String viewHomePage(ModelMap model) {
		return findPaginated(1, "result_id", "desc","", model);
	}
	
	@GetMapping("/listen/{resultId}")
	public String getListeningHistoryTestExam(
			ModelMap model,
			@PathVariable("resultId") Long resultId
			) {
		
		List<Integer> number = new ArrayList<>();
		for (int i = 1; i <= 50; ++i) {
			number.add(i);
		}
		model.addAttribute("number", number);
		model.addAttribute("resultId", resultId);
		model.addAttribute("list", historyExamTestService.getListHistoryListen(resultId));
		return "/teacher/histotytestresult/infoListenHistory";
	}
	
	
	@GetMapping("/reading/{resultId}")
	public String getReadingHistoryTestExam(
			ModelMap model,
			@PathVariable("resultId") Long resultId
			) {
		
		List<Integer> number = new ArrayList<>();
		
		for (int i = 51; i <= 100; ++i) {
			number.add(i);
		}
		
		model.addAttribute("number", number);
		model.addAttribute("resultId", resultId);
		model.addAttribute("list", historyExamTestService.getListHistoryReading(resultId));
		return "/teacher/histotytestresult/infoReadingHistory";
	}
	
	@GetMapping("/page/{pageNo}")
	public String findPaginated(
			@PathVariable(value = "pageNo") int pageNo, 
			@RequestParam("sortField") String sortField,
			@RequestParam("sortDir") String sortDir, 
			@RequestParam("searchName") String userName,
			ModelMap model) {
		
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String username = principal.toString();

			if (principal instanceof UserDetails) {
				username = ((UserDetails) principal).getUsername();
			}
			User currentUser = userService.findByUsernameLike(username);
			int pageSize = 5;
		
			Page<TestResult> page = testResultService.searchPageable(currentUser.getId(),userName, pageNo, pageSize, sortField, sortDir);
			List<TestResult> list = page.getContent();
			model.addAttribute("currentPage", pageNo);
			model.addAttribute("pageSize", pageSize);
			model.addAttribute("itemStart", pageNo * pageSize);
			model.addAttribute("totalPages", page.getTotalPages());
			model.addAttribute("totalItems", page.getTotalElements());
			model.addAttribute("sortField", sortField);
			model.addAttribute("sortDir", sortDir);
			model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
			model.addAttribute("list", list);
			model.addAttribute("id", currentUser.getId());
			model.addAttribute("searchName",userName);
			return "/teacher/histotytestresult/history";
	}
}
