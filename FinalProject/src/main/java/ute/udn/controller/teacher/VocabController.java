package ute.udn.controller.teacher;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFShape;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import ute.udn.entities.ContentListeningExcercise;
import ute.udn.entities.ContentVocabulary;
import ute.udn.entities.ListeningExcercise;
import ute.udn.entities.User;
import ute.udn.entities.Vocabulary;
import ute.udn.model.ConListeningDto;
import ute.udn.model.ConVocabularyDto;
import ute.udn.model.VocabularyDto;
import ute.udn.service.ConVocabularyService;
import ute.udn.service.UserService;
import ute.udn.service.VocabularyService;
import ute.udn.util.VocabularyExporter;
import ute.udn.util.SaveFile;

@Controller
@RequestMapping("/teacher/vocabulary")
public class VocabController {

	@Autowired
	VocabularyService service;

	@Autowired
	ConVocabularyService conService;
	
	@Autowired
	UserService userService;

	@GetMapping("")
	public String viewHomePage(ModelMap model) {
		return readingFindPaginated(1, "vocabId", "asc", "", model);
	}

//	add vocab
	@GetMapping("/add")
	public String add(ModelMap model) {
		model.addAttribute("vocab", new VocabularyDto());
		return "/teacher/vocabulary/saveVocab";
	}

//	save vocab
	@PostMapping("/save")
	public String save(ModelMap model,
			@Valid @ModelAttribute("vocab") VocabularyDto dto,BindingResult result,
			@RequestParam("file_imageQuestion") MultipartFile file,
			@RequestParam("file_Excel") MultipartFile fileExcel
			) throws IOException {
		try {
			if(result.hasErrors()) {
				return "/teacher/vocabulary/saveVocab";
			}
			if(fileExcel.isEmpty()) {
				model.addAttribute("error", "Chưa upload file excel từ vựng!");
				return "/teacher/vocabulary/saveVocab";
			}
			
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String username = principal.toString();

			if (principal instanceof UserDetails) {
				username = ((UserDetails) principal).getUsername();
			}
			User currentUser = userService.findByUsernameLike(username);
			
			//setImage
			UUID uuid = UUID.randomUUID();
			String file_Image = uuid.toString() + StringUtils.cleanPath(file.getOriginalFilename());// change name image
			String uploadDirImage = "src\\main\\resources\\static\\resources\\file\\images\\tuvung\\";// dir
			String fullPath = "/resources/file/images/tuvung/" + file_Image; // save path image in db
			SaveFile.saveFile(uploadDirImage, file_Image, file);
			dto.setImage(fullPath);
			
			//save vocabulary
			Vocabulary entity = new Vocabulary();
			BeanUtils.copyProperties(dto, entity);
			entity.setUsers(currentUser);
			service.save(entity);
			
			
			 //read file excel
			try {
				List<ContentVocabulary> listCauHoi = new ArrayList<>();
				Workbook workbook = new XSSFWorkbook(fileExcel.getInputStream());
				Sheet datatypeSheet = workbook.getSheetAt(0);
				DataFormatter fmt = new DataFormatter();

				Iterator<Row> iterator = datatypeSheet.iterator();
				XSSFDrawing dp = (XSSFDrawing) datatypeSheet.createDrawingPatriarch();
				List<XSSFShape> pics = dp.getShapes();

				while (iterator.hasNext()) {
					Random random = new Random();
					int rd = random.nextInt(100);
					String uuString = "image_" + uuid.toString();
					Row currentRow = iterator.next();
					ContentVocabulary conVocabulary = new ContentVocabulary();
					// save name of audio,excel
					conVocabulary.setVocab(fmt.formatCellValue(currentRow.getCell(0)));
					conVocabulary.setSpelling(fmt.formatCellValue(currentRow.getCell(1)));
					for (Iterator<? extends XSSFShape> it = pics.iterator(); it.hasNext();) {
						XSSFPicture inpPic = (XSSFPicture) it.next();
						XSSFClientAnchor clientAnchor = inpPic.getClientAnchor();
						PictureData pict = inpPic.getPictureData();
						byte[] data = pict.getData();

						if (clientAnchor.getCol1() == 2 && clientAnchor.getRow1() == currentRow.getRowNum()) {
							byte[] imageData = data;
							String pathImageInExcel = "src\\main\\resources\\static\\resources\\file\\images\\tuvung\\";
							FileOutputStream out = new FileOutputStream(pathImageInExcel + uuString + rd + ".png");
							conVocabulary.setImages("/resources/file/images/tuvung/" + uuString + rd + ".png");
							out.write(imageData);
							out.close();
						}
					}
					conVocabulary.setAudio("/resources/file/audio/audiotuvung/"+fmt.formatCellValue(currentRow.getCell(3)));
					conVocabulary.setMean(fmt.formatCellValue(currentRow.getCell(4)));
					conVocabulary.setSentence(fmt.formatCellValue(currentRow.getCell(5)));

					conVocabulary.setVocabulary(entity);
					listCauHoi.add(conVocabulary);
					conService.save(conVocabulary);
				}
				workbook.close();
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}

			model.addAttribute("message", "Thêm mới bài từ vựng thành công!");
			return readingFindPaginated(1, "vocabId", "asc", "", model);
		} catch (Exception e) {
			model.addAttribute("error", "Thêm mới bài từ vựng thất bại!");
			return readingFindPaginated(1, "vocabId", "asc", "", model);
		}

	}

//	delete vocab
	@GetMapping("/delete-id={vocabId}")
	public ModelAndView delete(ModelMap model, @PathVariable("vocabId") Long vocabId) {
		try {
			service.deleteById(vocabId);
			model.addAttribute("message", "Xóa thành công");
			return new ModelAndView("forward:/teacher/vocabulary", model);
		} catch (Exception e) {
			model.addAttribute("error", "Bài từ vựng không tồn tại trên hệ thống");
			return new ModelAndView("forward:/teacher/vocabulary", model);
		}

	}

	@GetMapping("/page/{pageNo}")
	public String readingFindPaginated(@PathVariable(value = "pageNo") int pageNo,
			@RequestParam("sortField") String sortField, @RequestParam("sortDir") String sortDir,
			@RequestParam("searchName") String searchName, ModelMap model) {
		
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = principal.toString();
		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		}
		User currentUser = userService.findByUsernameLike(username);
		
		int pageSize = 5;
		Page<Vocabulary> page = service.searchPageableByUserId(currentUser.getId(),pageNo, pageSize, sortField, sortDir, searchName);
		List<Vocabulary> vocabularies = page.getContent();
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("itemStart", pageNo * pageSize);
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
		model.addAttribute("vocabularies", vocabularies);
		model.addAttribute("searchName", searchName);
		model.addAttribute("id", currentUser.getId());
		
		return "/teacher/vocabulary/listVocabulary";
	}

	@GetMapping("/edit-id={vocabId}")
	public String update(ModelMap model, @PathVariable("vocabId") long vocabId) {
		VocabularyDto dto = new VocabularyDto();
		Vocabulary entity = service.getVocabularyById(vocabId);
		BeanUtils.copyProperties(entity, dto);
		model.addAttribute("id", vocabId);
		model.addAttribute("vocab", dto);
		return "/teacher/vocabulary/updateVocab";
	}

	@PostMapping("/update")
	public String update(ModelMap model, @Valid @ModelAttribute("vocab") VocabularyDto dto,
			@RequestParam("file_imageQuestion") MultipartFile file, BindingResult result) throws IOException {

		try {
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String username = principal.toString();

			if (principal instanceof UserDetails) {
				username = ((UserDetails) principal).getUsername();
			}
			User currentUser = userService.findByUsernameLike(username);
			// upload file image
			UUID uuid = UUID.randomUUID();
			String file_Image = uuid.toString() + StringUtils.cleanPath(file.getOriginalFilename());// change name image
			String uploadDirImage = "src\\main\\resources\\static\\resources\\file\\images\\tuvung\\";// dir
			String fullPath = "/resources/file/images/tuvung/" + file_Image; // save path image in db
			
			// kiểm tra file ảnh đã tồn tại trên hệ thống chưa
			Vocabulary entity = new Vocabulary();
			if (file.isEmpty()) {
				entity.setImage(dto.getImage());
			} else {
				dto.setImage(fullPath);
			}
			
			SaveFile.saveFile(uploadDirImage, file_Image, file);
			BeanUtils.copyProperties(dto, entity);
			entity.setUsers(currentUser);
			service.save(entity);
			model.addAttribute("message", "Sửa thông tin bài từ vựng thành công!");
			return readingFindPaginated(1, "vocabId", "asc", "", model);
		} catch (Exception e) {
			model.addAttribute("error", "Sửa thông tin bài từ vựng thất bại!");
			return readingFindPaginated(1, "vocabId", "asc", "", model);
		}

	}

	

//	====== Content Vocabulary =======

	@GetMapping("/info-id={vocabId}/page/{pageNo}")
	public String findPaginated(
			@PathVariable(value = "vocabId") long vocabId,
			@PathVariable(value = "pageNo") int pageNo,
			ModelMap model) {
		int pageSize = 5;
		Page<ContentVocabulary> page = conService.pageable(pageNo, pageSize, vocabId);
		List<ContentVocabulary> vocab = page.getContent();
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("itemStart", pageNo * pageSize);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("vocab", vocab);
		model.addAttribute("detail", service.getVocabularyById(vocabId));
		return "/teacher/vocabulary/listContentVocabulary";
	}
	
	
	
	@GetMapping("/add-new-vocabulary={id}")
	public String addQuestion(ModelMap model, @PathVariable("id") Long ids) {
		model.addAttribute("idVocab", ids);
		model.addAttribute("vocab", new ConVocabularyDto());
		return "/teacher/vocabulary/saveOrEditVocabulary";
	}
	
	
	

	@PostMapping("/save-new-vocabulary")
	public String saveOrUpdateQuestion(
			ModelMap model, 
			@Valid @ModelAttribute("vocab") ConVocabularyDto dto,
			BindingResult result, @RequestParam("file_imageQuestion") MultipartFile file,
			@RequestParam("file_listen") MultipartFile fileAudio,
			@RequestParam("idVocab") Long id) {
		try {
			if (result.hasErrors()) {
				model.addAttribute("idVocab",id);
				return "/teacher/vocabulary/saveOrEditVocabulary";
			}
			ContentVocabulary entity = new ContentVocabulary();
			UUID uuid = UUID.randomUUID();
			// upload file image
			String file_Image = uuid.toString() + StringUtils.cleanPath(file.getOriginalFilename());// change name image
			String uploadDirImage = "src\\main\\resources\\static\\resources\\file\\images\\tuvung\\";// dir
			String fullPath = "/resources/file/images/tuvung/" + file_Image; // save path image in db
			
			//lưu ảnh
			SaveFile.saveFile(uploadDirImage, file_Image, file);
			
			if(dto.getIsEdit()==true) {
				entity.setImages(dto.getImages());
			}else {
				dto.setImages(fullPath);
			}
			
			// save file audio
			String file_Audio = uuid.toString() + StringUtils.cleanPath(fileAudio.getOriginalFilename());// change name
			String uploadDirAudio = "src\\main\\resources\\static\\resources\\file\\audio\\bainghe\\";
			String fullPathAudio = "/resources/file/audio/bainghe/" + file_Audio; // save path to db
			SaveFile.savePathFile(fileAudio, file_Audio, uploadDirAudio);
			
			if(dto.getIsEdit()==true) {
				entity.setAudio(dto.getAudio());
			}else {
				dto.setAudio(fullPathAudio);
			}
			

			Vocabulary vo = new Vocabulary();
			vo.setVocabId(id);
			dto.setVocabulary(vo);

			
			BeanUtils.copyProperties(dto, entity);
			// save ListeningExcercise
			conService.save(entity);
			
			
			if(dto.getIsEdit()==false)
			{
				model.addAttribute("message", "Thêm mới từ vựng thành công!");
			}else {
				model.addAttribute("message", "Sửa từ vựng thành công!");
			}
			
//			return "/teacher/vocabulary/info-id="+id+"/page/1";
			return findPaginated(id, 1, model);
		} catch (Exception e) {
			model.addAttribute("error", "Thêm mới từ vựng thất bại!");
			return findPaginated(id, 1, model);
		}

		

	}
	

	@GetMapping("edit-vocabulary-id={vocabId}/{id}")
	public String editContentVocabulary(ModelMap model, 
			@PathVariable("vocabId") Long vocabId,
			@PathVariable("id") Long ids) {
		ConVocabularyDto dto = new ConVocabularyDto();
		dto.setIsEdit(true);
		model.addAttribute("idVocab", ids);

		ContentVocabulary entity = conService.getVocabularyById(vocabId);
		BeanUtils.copyProperties(entity, dto);
		model.addAttribute("vocab", dto);
		return "/teacher/vocabulary/saveOrEditVocabulary";
	}

	@GetMapping("/delete-vocabulary-id={cVocabId}/{id}")
	public String deleteContentVocabulary(ModelMap model, 
			@PathVariable("cVocabId") Long cVocabId,
			@PathVariable("id") Long ids) {

		try {
			conService.deleteById(cVocabId);
			model.addAttribute("message", "Xóa thành công!");
			return findPaginated(ids, 1, model);

		} catch (Exception e) {
			model.addAttribute("error", "Từ vựng có ID= " +cVocabId+ " không tồn tại trên hệ thống!");
			return findPaginated(ids, 1, model);
		}

	}
	
	

	@GetMapping("/export/excel/{vocabId}")
	public void exportToExcel(
			@PathVariable("vocabId") Long vocabId,
			HttpServletResponse response) throws IOException {

		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());

		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=vocabulary_ID_"+vocabId+ "_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<ContentVocabulary> listVocab = conService.getListContenVocabularyByVocabId(vocabId);
		VocabularyExporter excelExporter = new VocabularyExporter(listVocab);
		excelExporter.export(response);
	}

}
