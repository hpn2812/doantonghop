package ute.udn.controller.teacher;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.PictureData;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFPicture;
import org.apache.poi.xssf.usermodel.XSSFShape;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import ute.udn.entities.ContentListeningExcercise;
import ute.udn.entities.ContentReadingExcercise;
import ute.udn.entities.ContentVocabulary;
import ute.udn.entities.Grammar;
import ute.udn.entities.ListeningExcercise;
import ute.udn.entities.TestExam;
import ute.udn.entities.TestExamQuestion;
import ute.udn.entities.User;
import ute.udn.model.ConListeningDto;
import ute.udn.model.ConReadingDto;
import ute.udn.model.ListeningExcerciseDto;
import ute.udn.model.TestExamDto;
import ute.udn.model.TestExamQuestionDto;
import ute.udn.model.UserDto;
import ute.udn.service.TestExamQuestionService;
import ute.udn.service.TestExamService;
import ute.udn.service.UserService;
import ute.udn.util.SaveFile;
import ute.udn.util.TestQuestionExporter;
import ute.udn.util.VocabularyExporter;

@Controller
@RequestMapping("/teacher/test-exam")
public class ExamTestController {
	
	@Autowired
	TestExamService testExamService;
	
	@Autowired
	TestExamQuestionService testExamQuestionService;
	
	@Autowired
	UserService userService;
	
	
	@GetMapping("")
	public String getTestExam(ModelMap model) {
		return findPaginated(1, "testExamId", "asc","", model);
	}
	
	@GetMapping("add")
	public String addTestExam(ModelMap model) {
		model.addAttribute("testExam", new TestExamDto());
		return "/teacher/testexam/saveTestExam";
	}
	
	@PostMapping("/save")
	public String saveOrUpdate(ModelMap model, 
			@Valid @ModelAttribute("testExam") TestExamDto dto,BindingResult result, 
			@RequestParam("file_imageQuestion") MultipartFile file,
			@RequestParam("file_Excel") MultipartFile fileExcel ) {
		
		try {
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String username = principal.toString();

			if (principal instanceof UserDetails) {
				username = ((UserDetails) principal).getUsername();
			}
			User currentUser = userService.findByUsernameLike(username);
			
			if (result.hasErrors()) {
				return "/teacher/testexam/saveTestExam";
			} else {
				if(fileExcel.isEmpty()) {
					model.addAttribute("error", "Vui lòng upload file excel câu hỏi!");
					return "/teacher/testexam/saveTestExam";
				}
				UUID uuid = UUID.randomUUID();
				// upload file image
				String file_Image = uuid.toString() + StringUtils.cleanPath(file.getOriginalFilename());// change name image
				String uploadDirImage = "src\\main\\resources\\static\\resources\\file\\images\\baithi\\";// dir
				String fullPath = "/resources/file/images/baithi/" + file_Image; // save path image in db
				SaveFile.saveFile(uploadDirImage, file_Image, file);
				dto.setImage(fullPath);
				TestExam entity= new TestExam();
				BeanUtils.copyProperties(dto, entity);
				
				entity.setUsers(currentUser);
				// save Test Exam
				testExamService.save(entity);

				// read file excel
				try {
					List<TestExamQuestion> listCauHoi = new ArrayList<>();
					Workbook workbook = new XSSFWorkbook(fileExcel.getInputStream());
					Sheet datatypeSheet = workbook.getSheetAt(0);
					DataFormatter fmt = new DataFormatter();
					Iterator<Row> iterator = datatypeSheet.iterator();
					XSSFDrawing dp = (XSSFDrawing) datatypeSheet.createDrawingPatriarch();
					List<XSSFShape> pics = dp.getShapes();
					while (iterator.hasNext()) {
						Row currentRow = iterator.next();
						TestExamQuestion testExamQuestion = new TestExamQuestion();
						if("".equalsIgnoreCase(fmt.formatCellValue(currentRow.getCell(0)).toString().trim())==false) {
							testExamQuestion.setAudio("/resources/file/images/baithi/audio/" + fmt.formatCellValue(currentRow.getCell(0)));
						}else {
							testExamQuestion.setAudio("");
						}
						
						testExamQuestion.setPragraph(fmt.formatCellValue(currentRow.getCell(1)));
						testExamQuestion.setQuestion(fmt.formatCellValue(currentRow.getCell(2)));
						testExamQuestion.setOption1(fmt.formatCellValue(currentRow.getCell(3)));
						testExamQuestion.setOption2(fmt.formatCellValue(currentRow.getCell(4)));
						testExamQuestion.setOption3(fmt.formatCellValue(currentRow.getCell(5)));
						testExamQuestion.setOption4(fmt.formatCellValue(currentRow.getCell(6)));
						testExamQuestion.setCorectOption(fmt.formatCellValue(currentRow.getCell(7)));
						// read image in file excel
						for (Iterator<? extends XSSFShape> it = pics.iterator(); it.hasNext();) {
							XSSFPicture inpPic = (XSSFPicture) it.next();
							XSSFClientAnchor clientAnchor = inpPic.getClientAnchor();
							PictureData pict = inpPic.getPictureData();
							byte[] data = pict.getData();

							String uuString = "image_" + uuid.toString();

							if (clientAnchor.getCol1() == 8 && clientAnchor.getRow1() == currentRow.getRowNum()) {
								byte[] imageData = data;

								String pathImageInExcel = "src\\main\\resources\\static\\resources\\file\\images\\baithi\\cauhoibaithi\\";
								FileOutputStream out = new FileOutputStream(pathImageInExcel + uuString + ".png");
								String fullPathImageQuestion = "/resources/file/images/baithi/cauhoibaithi/" + uuString+".png";
								out.write(imageData);
								out.close();
								testExamQuestion.setImage(fullPathImageQuestion);
							}
						}
						testExamQuestion.setChose("e");
						testExamQuestion.setTestExams(entity);
						listCauHoi.add(testExamQuestion);
						testExamQuestionService.save(testExamQuestion);
					}
					workbook.close();
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}
				
				model.addAttribute("message", "Thêm mới đề thi thành công!");
				return findPaginated(1, "testExamId", "asc","", model);

			}
			
		} catch (Exception e) {
			model.addAttribute("error", "Thêm mới đề thi thất bại!");
			return findPaginated(1, "testExamId", "asc","", model);
		}
	}
	
	
	@GetMapping("/edit-id={testExamId}")
	public String edit(ModelMap model, @PathVariable("testExamId") Long id) {
		TestExamDto dto = new TestExamDto();
		TestExam entity = testExamService.getTestExamById(id);
		BeanUtils.copyProperties(entity, dto);
		model.addAttribute("id", id);
		model.addAttribute("testExam", dto);
		return "teacher/testexam/updateTestExam";
	}
	
	
	@PostMapping("/update")
	public String update(ModelMap model,
			@Valid @ModelAttribute("testExam") TestExamDto dto,
			@RequestParam("file_imageQuestion") MultipartFile file,
			BindingResult result) {

			try {
				Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				String username = principal.toString();

				if (principal instanceof UserDetails) {
					username = ((UserDetails) principal).getUsername();
				}
				User currentUser = userService.findByUsernameLike(username);
				
				TestExam entity = new TestExam();
				UUID uuid = UUID.randomUUID();
				String file_Image = uuid + StringUtils.cleanPath(file.getOriginalFilename());

				if (file.isEmpty()) {
					entity.setImage(dto.getImage());
				} else {
					// upload file image

					file_Image = uuid.toString() + StringUtils.cleanPath(file.getOriginalFilename());// change name image

					String uploadDirImage = "src\\main\\resources\\static\\resources\\file\\images\\baithi\\";// dir

					String fullPath = "/resources/file/images/baithi/" + file_Image; // save path image in db

					SaveFile.saveFile(uploadDirImage, file_Image, file); // save file to source
					dto.setImage(fullPath);
				}
				BeanUtils.copyProperties(dto, entity);
				entity.setUsers(currentUser);
				
				testExamService.save(entity);
				model.addAttribute("message", "Cập nhật đề thi thành công!");
				return findPaginated(1, "testExamId", "asc","", model);
			} catch (Exception e) {
				model.addAttribute("error", "Cật nhật đề thi thất bại!");
				return findPaginated(1, "testExamId", "asc","", model);
			}
			
	
	}
	
	@GetMapping("/delete-id={testExamId}")
	public String delete(ModelMap model, @PathVariable("testExamId") Long testExamId) {
		try {
			testExamService.deleteById(testExamId);
			model.addAttribute("message", "Xóa đề thi thành công!");
			return findPaginated(1, "testExamId", "asc","", model);
			
		} catch (Exception e) {
			model.addAttribute("error", "Đề thi id= " +testExamId+" không tồn tại trên hệ thống!");
			return findPaginated(1, "testExamId", "asc","", model);
		}
		
	}
	
	
	
	@GetMapping("/page/{pageNo}")
	public String findPaginated(
			@PathVariable(value = "pageNo") int pageNo, 
			@RequestParam("sortField") String sortField,
			@RequestParam("sortDir") String sortDir,
			@RequestParam("searchName") String searchName,
			ModelMap model) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = principal.toString();

		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		}
		User currentUser = userService.findByUsernameLike(username);
		
		int pageSize = 5;
		Page<TestExam> page = testExamService.searchPageableByUserId(currentUser.getId(),pageNo, pageSize, sortField, sortDir,searchName);
		List<TestExam> testExam = page.getContent();
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("itemStart", pageNo * pageSize);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
		model.addAttribute("testExam", testExam);
		model.addAttribute("searchName", searchName);
		return "/teacher/testexam/listTestExam";
	}

		
//	================Test Question =================
		
	@GetMapping("/info-id={testExamId}/page/{pageNo}")
	public String findPaginated(
			@PathVariable(value = "testExamId") long testExamId,
			@PathVariable(value = "pageNo") int pageNo,
			ModelMap model) {
		int pageSize = 10;
		Page<TestExamQuestion> page = testExamQuestionService.pageable(testExamId, pageNo, pageSize);
		List<TestExamQuestion> testExamDetail = page.getContent();
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("itemStart", pageNo * pageSize);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("testExamId", testExamId);
		model.addAttribute("testExamDetail", testExamDetail);
		return "/teacher/testexam/listQuestion";
	}
	
	
	
	@GetMapping("/add-test-question={id}")
	public String addQuestion(ModelMap model,
			@PathVariable("id") Long id) {
		model.addAttribute("id", id);
		model.addAttribute("listQuestion", new TestExamQuestionDto());
		return "/teacher/testexam/saveOrEditQuestion";
	}
	
	@PostMapping("/save-test-question")
	public String saveOrUpdateQuestion(ModelMap model,
			@Valid @ModelAttribute("listQuestion") TestExamQuestionDto dto,
			BindingResult result, 
			@RequestParam("file_imageQuestion") MultipartFile file,
			@RequestParam("file_listen") MultipartFile fileAudio, 
			@RequestParam("id") Long id) {
		try {
			
			if (result.hasErrors()) {
				return "/teacher/testexam/saveOrEditQuestion";
			}
			TestExam testExam = new TestExam();
			TestExamQuestion  testExamQuestion = new TestExamQuestion();
			UUID uuid = UUID.randomUUID();
			
			// check file image đã tồn tại chưa : rồi thì bỏ qua chưa thì lưu mới
			if(file.isEmpty()) {
				testExamQuestion.setImage(dto.getImage());
			}else {
				String file_Image = uuid.toString() + StringUtils.cleanPath(file.getOriginalFilename());// change name image
				String uploadDirImage = "src\\main\\resources\\static\\resources\\file\\images\\baithi\\cauhoibaithi\\";// dir
				String fullPath = "/resources/file/images/baithi/cauhoibaithi/" + file_Image; // save path image in db
				SaveFile.saveFile(uploadDirImage, file_Image, file);
				dto.setImage(fullPath);
				
			}
			
			// check file audio đã tồn tại chưa : rồi thì bỏ qua chưa thì lưu mới
			if (fileAudio.isEmpty()) {
				testExamQuestion.setAudio(dto.getAudio());
			} else {
				String file_Audio = uuid.toString() + StringUtils.cleanPath(fileAudio.getOriginalFilename());// change name
				String uploadDirAudio = "src\\main\\resources\\static\\resources\\file\\images\\baithi\\audio\\";
				String fullPathAudio = "/resources/file/images/baithi/audio/" + file_Audio; // save path to db
				SaveFile.savePathFile(fileAudio, file_Audio, uploadDirAudio);
				dto.setAudio(fullPathAudio);
			}	
			testExam.setTestExamId(id);
			dto.setTestExams(testExam);
			BeanUtils.copyProperties(dto, testExamQuestion);
			testExamQuestionService.save(testExamQuestion);
			model.addAttribute("message", "Thêm câu hỏi thành công!");
			return findPaginated(id, 1, model);
		} catch (Exception e) {
			model.addAttribute("error", "Thêm câu hỏi thất bại!");
			return findPaginated(id, 1, model);
		}	
	}
	
	
	
	@GetMapping("/edit-test-question={questionId}/{testExamId}")
	public String editReadingQuestion(ModelMap model,
			@PathVariable("questionId") Long questionId,
			@PathVariable("testExamId") Long testExamId
			) {
		
		TestExamQuestionDto dto = new TestExamQuestionDto();
		dto.setIsEdit(true);
		model.addAttribute("id", testExamId);
		TestExamQuestion entity = testExamQuestionService.getTestQuestionById(questionId);
		BeanUtils.copyProperties(entity, dto);
		model.addAttribute("listQuestion", dto);
		model.addAttribute("corectOption",entity.getCorectOption());
		return "/teacher/testexam/saveOrEditQuestion";
	}
	
	@GetMapping("/delete-test-question={questionId}/{testExamId}")
	public String deleteQuestion(ModelMap model, 
			@PathVariable("questionId") Long questionId,
			@PathVariable("testExamId") Long testExamId){
		
		try {
			testExamQuestionService.deleteById(questionId);
			model.addAttribute("message", "Xóa thành công!");
			return findPaginated(testExamId, 1, model);
		} catch (Exception e) {
			model.addAttribute("error", "Không tồn tại câu hỏi có ID = " + questionId + " trên hệ thống");
			return findPaginated(testExamId, 1, model);
		}

	}
	
	
	@GetMapping("/export/excel/{testExamId}")
	public void exportToExcel(
			@PathVariable("testExamId") Long testExamId,
			HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=Question_Test_ID_"+testExamId+"_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<TestExamQuestion> list = testExamQuestionService.getListTestExamQuestionById(testExamId);
		TestQuestionExporter excelExporter = new TestQuestionExporter(list);
		excelExporter.export(response);
	}
}
