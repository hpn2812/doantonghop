package ute.udn.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import ute.udn.entities.Role;
import ute.udn.model.RoleDto;
import ute.udn.service.RoleService;

@Controller
@RequestMapping("/admin/role")
public class RoleController {
	
	@Autowired
	RoleService roleService;
	
	@GetMapping("")
	public String homePage(ModelMap model) {
		model.addAttribute("list", roleService.getRole());
		return "/admin/role/home";
	}
	@GetMapping("/add")
	public String add(ModelMap model) {
		model.addAttribute("roledto", new RoleDto());
		return "/admin/role/saveOrEdit";
		
	}
	@PostMapping("/saveOrEdit")
	public String saveOrUpdate(ModelMap model,
		@Valid	@ModelAttribute("roledto") RoleDto dto,
		BindingResult result) {
		try {
			
			Role entity= new Role();
			BeanUtils.copyProperties(dto, entity);
			roleService.save(entity);
			
			model.addAttribute("list", roleService.getRole());
			if(dto.getIsEdit()==false) {
				model.addAttribute("message", "Thêm mới thành công");
			}else {
				model.addAttribute("message", "Cập nhật thành công");
			}
			
			return "/admin/role/home";
			
		} catch (Exception e) {
			// TODO: handle exception
			if(dto.getIsEdit()==false) {
				model.addAttribute("error", "Thêm mới thất bại");
			}else {
				model.addAttribute("error", "Cập nhật thất bại");
			}
			return "/admin/role/home";
		}
	}
	
	@GetMapping("/edit-id={id}")
	public String edit(ModelMap model, @PathVariable("id") Long id) {
		RoleDto dto = new RoleDto();
		dto.setIsEdit(true); // set update = true
		Role entity = roleService.getRoleById(id);
		BeanUtils.copyProperties(entity, dto);
		model.addAttribute("roledto", dto);
		return "/admin/role/saveOrEdit";

	}
	
	
	@GetMapping("/delete-id={id}")
	public String delete(ModelMap model, @PathVariable("id") Long id) {
		try {
			roleService.deleteById(id);
			model.addAttribute("list", roleService.getRole());
			model.addAttribute("message", "Xóa vai trò thành công");
			return "/admin/role/home";
		} catch (Exception e) {
			model.addAttribute("list", roleService.getRole());
			model.addAttribute("error", "Xóa vai trò thất bại");
			return "/admin/role/home";
		}
		

	}


}
