package ute.udn.controller.admin;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ute.udn.entities.ListeningExcercise;
import ute.udn.entities.User;
import ute.udn.model.UserDto;
import ute.udn.service.RoleService;
import ute.udn.service.UserService;

@Controller
@RequestMapping("/admin/account")
public class AccountController {

	@Autowired
	UserService userService;

	@Autowired
	RoleService roleService;

	// Xong
//	@GetMapping("")
//	public String viewHomePage(ModelMap model) {
//		return findPaginated(1, "id", "asc", model);
//	}
	long a = 0;

	@GetMapping("")
	public String viewHomePage(ModelMap model) {

		return searchPageable(1, "id", "asc", a, model);
	}

	@GetMapping("/add")
	public String add(ModelMap model) {
		model.addAttribute("user", new UserDto());
		model.addAttribute("listUserRoleId", roleService.getRole());
		return "admin/account/saveOrEditAccount";
	}

	@PostMapping("/saveOrEdit")
	public String saveOrUpdate(ModelMap model, @Valid @ModelAttribute("user") UserDto dto, BindingResult result,
			@RequestParam("listUserRoleId") List<Long> listUserRoleId) {

		User existing = userService.findByUsernameLike(dto.getUsername());
		if (existing != null) {
			model.addAttribute("error", "Tên đăng nhập đã tồn tại trên hệ thống");
			model.addAttribute("listUserRoleId", roleService.getRole());
			return "admin/account/saveOrEditAccount";
		}
		if (result.hasErrors()) {
			model.addAttribute("listUserRoleId", roleService.getRole());
			return "admin/account/saveOrEditAccount";
		} else {
			dto.setEnabled(true);
			dto.getPassword().trim();
			User user = new User();
			BeanUtils.copyProperties(dto, user);
			userService.save(user);
			user.setRoles(roleService.getListUserRoles(listUserRoleId));
			userService.saveAndFlush(user);

		}
		return "redirect:/admin/account";

	}

	@GetMapping("/edit-id={id}")
	public String edit(ModelMap model, @PathVariable("id") Long id) {
		UserDto dto = new UserDto();
		dto.setIsEdit(true); // set update = true
		User entity = userService.getUserById(id);
		entity.setEnabled(false);
		BeanUtils.copyProperties(entity, dto);
		model.addAttribute("user", dto);
		model.addAttribute("listUserRoleId", roleService.getRole());

		return "admin/account/saveOrEditAccount";
	}

	@GetMapping("/block-id={id}")
	public String blockAccount(ModelMap model, @PathVariable("id") Long id) {
		try {
			User entity = userService.getUserById(id);
			entity.setEnabled(false);
			userService.updateUser(entity);
			model.addAttribute("message", "Khóa tài khoản thành công!");
			return searchPageable(1, "id", "asc", a, model);
		} catch (Exception e) {
			// TODO: handle exception
			model.addAttribute("error", "Khóa tài khoản thất bại!");
			return searchPageable(1, "id", "asc", a, model);
		}

	}

	@GetMapping("/active-id={id}")
	public String acitveAccount(ModelMap model, @PathVariable("id") Long id) {
		try {
			User entity = userService.getUserById(id);
			entity.setEnabled(true);
			userService.updateUser(entity);
			model.addAttribute("message", "Mở khóa tài khoản thành công!");
			return searchPageable(1, "id", "asc", a, model);
		} catch (Exception e) {
			// TODO: handle exception
			model.addAttribute("error", "Mở khóa tài khoản thất bại!");
			return searchPageable(1, "id", "asc", a, model);
		}
	}

	@GetMapping("/search")
	public String search(ModelMap model, @RequestParam(name = "search", required = false) long search) {
		
		List<User> list = userService.search(search);

		model.addAttribute("accounts", list);
		model.addAttribute("search", search);
		
		model.addAttribute("userService", userService);
		return "/admin/account/listAccount";

	}

	@GetMapping("/page/{pageNo}")
	public String searchPageable(@PathVariable(value = "pageNo") int pageNo,
			@RequestParam("sortField") String sortField, @RequestParam("sortDir") String sortDir,
			@RequestParam("search") Long search, ModelMap model) {
		int pageSize = 5;
		Page<User> page = userService.searchPageable(pageNo, pageSize, sortField, sortDir, search);
		List<User> user = page.getContent();
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("itemStart", pageNo * pageSize);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
		model.addAttribute("accounts", user);
		model.addAttribute("userService", userService);
		model.addAttribute("search", search);
		model.addAttribute("listUserRoleId", roleService.getRole());
		return "/admin/account/listAccount";
	}

}
