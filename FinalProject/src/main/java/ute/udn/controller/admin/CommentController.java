package ute.udn.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ute.udn.entities.CommentGrammar;
import ute.udn.entities.User;
import ute.udn.entities.Vocabulary;
import ute.udn.service.CommentGrammarService;

@Controller
@RequestMapping("/admin/comment")
public class CommentController {
	
	
	@Autowired
	CommentGrammarService service;
	
	@GetMapping("")
	public String getManagerComment(ModelMap model) {

		return commentFindPaginated(1, "cGramId", "desc","",0,0,model);
	}
	
	@GetMapping("/block-id={cGramId}")
	public String hiddenComment(
			ModelMap model,
			@PathVariable("cGramId") Long cGramId) {
		
		try {
			CommentGrammar commentGrammar=service.getCommentById(cGramId);
			commentGrammar.setDisplay(2);
			service.save(commentGrammar);
			model.addAttribute("message", "Ẩn bình luận thành công!");
			return commentFindPaginated(1, "cGramId", "asc","",0,0,model);
			
		} catch (Exception e) {
			model.addAttribute("error", "Ẩn bình luận thất bại!");
			return commentFindPaginated(1, "cGramId", "asc","",0,0,model);
		}
		
	}
	
	@GetMapping("/watched={cGramId}")
	public String watchedComment(
			ModelMap model,
			@PathVariable("cGramId") Long cGramId) {
		
		try {
			CommentGrammar commentGrammar=service.getCommentById(cGramId);
			commentGrammar.setNewComment(2);
			service.save(commentGrammar);
			model.addAttribute("message", "Đánh dấu đã xem thành công");
			return commentFindPaginated(1, "cGramId", "asc","",0,0,model);
			
		} catch (Exception e) {
			model.addAttribute("error", "Đánh dấu đã xem thất bại!");
			return commentFindPaginated(1, "cGramId", "asc","",0,0,model);
		}
		
	}
	
	@GetMapping("/unblock-id={cGramId}")
	public String unHiddenComment(
			ModelMap model,
			@PathVariable("cGramId") Long cGramId) {
		try {
			CommentGrammar commentGrammar=service.getCommentById(cGramId);
			commentGrammar.setDisplay(1);
			service.save(commentGrammar);
			model.addAttribute("message", "Hiển thị bình luận thành công!");
			return commentFindPaginated(1, "cGramId", "asc","",0,0,model);
			
		} catch (Exception e) {
			model.addAttribute("error", "Hiển thị bình luận thất bại!");
			return commentFindPaginated(1, "cGramId", "asc","",0,0,model);
		}
		
	}
	
	
	
	@GetMapping("/page/{pageNo}")
	public String commentFindPaginated(
			@PathVariable(value = "pageNo") int pageNo,
			@RequestParam("sortField") String sortField, 
			@RequestParam("sortDir") String sortDir,
			@RequestParam("searchName") String searchName,
			@RequestParam("searchDisplay") Integer searchDisplay,
			@RequestParam("searchNewComment") Integer searchNewComment,
			ModelMap model) {
		
		
		int pageSize = 5;
		Page<CommentGrammar> page = service.searchPageable(pageNo, pageSize, sortField, sortDir, searchName, searchDisplay, searchNewComment);
		List<CommentGrammar> listComment = page.getContent();
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("itemStart", pageNo * pageSize);
		model.addAttribute("totalItems", page.getTotalElements());
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
		model.addAttribute("list", listComment);
		
		model.addAttribute("searchName", searchName);
		model.addAttribute("searchDisplay", searchDisplay);
		model.addAttribute("searchNewComment", searchNewComment);
		
		return "/admin/comment/listComment";
	}
	

}
