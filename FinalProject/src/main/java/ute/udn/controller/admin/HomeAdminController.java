package ute.udn.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ute.udn.service.CommentGrammarService;
import ute.udn.service.UserService;

@Controller
@RequestMapping("/admin")
public class HomeAdminController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	CommentGrammarService commentService;
	
	
	@GetMapping("")
	public String homePage(ModelMap model) {
		model.addAttribute("countNewComment", commentService.countNewComment(1));
		model.addAttribute("countMember", userService.countUserByRole(2));
		model.addAttribute("countTeacher", userService.countUserByRole(3));
		return "/admin/dashboard/home";
	}

}
