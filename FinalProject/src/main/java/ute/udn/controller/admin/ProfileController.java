package ute.udn.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ute.udn.entities.User;
import ute.udn.model.UserDto;
import ute.udn.service.UserService;
import ute.udn.util.Password;

@Controller
@RequestMapping("/admin/profile")
public class ProfileController {

	@Autowired
	UserService service;

	@Autowired
	private BCryptPasswordEncoder encode;

	@GetMapping(value = "")
	public String profilePage(ModelMap model) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String userName = principal.toString();
		if (principal instanceof UserDetails) {
			userName = ((UserDetails) principal).getUsername();
		}
		model.addAttribute("user", service.findByUsernameLike(userName));
		return "/admin/profile/updateProfile";
	}

	@GetMapping(value = "/change-password")
	public String changePasswordPage(ModelMap model) {
		return "/admin/profile/updatePassword";
	}

	@PostMapping("/update")
	public String saveOrUpdate(ModelMap model,
			@Valid @ModelAttribute("user") UserDto dto,
			BindingResult result
			
			) {
		try {
				Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				String userName = principal.toString();
				if (principal instanceof UserDetails) {
					userName = ((UserDetails) principal).getUsername();
				}
				if(result.hasErrors()) {
				
					return "/admin/profile/updateProfile";
				}else {
					User user = service.findByUsernameLike(userName);
					user.setFullName(dto.getFullName());
					user.setAddress(dto.getAddress());
					user.setPhoneNumber(dto.getPhoneNumber());
					service.updateUser(user);
					model.addAttribute("message", "Cập nhật thông tin thành công!");
					return "/admin/profile/updateProfile";
				}
				
	
		} catch (Exception e) {
			model.addAttribute("error", "Cập nhật thông tin thất bại!");
			return "/admin/profile/updateProfile";
		}

	}

	@PostMapping("/changePassword")
	public String changePassword(ModelMap model,
			@RequestParam("currentPassword") String currentPassword,
			@RequestParam("newPassword") String newPassword, 
			@RequestParam("reNewPassword") String reRewPassword) {
		try {

			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			String username = principal.toString();

			if (principal instanceof UserDetails) {
				username = ((UserDetails) principal).getUsername();
			}

			User user = service.findByUsernameLike(username);
			if ("".equalsIgnoreCase(currentPassword) || "".equalsIgnoreCase(newPassword)
					|| "".equalsIgnoreCase(reRewPassword)) {
				model.addAttribute("error", "Vui lòng điền đầy đủ thông tin!");
				return "/admin/profile/updatePassword";
			} else if (newPassword.equals(reRewPassword) == false) {
				model.addAttribute("error", "Nhắc lại mật khẩu mới không trùng khớp!");
				return "/admin/profile/updatePassword";
			}else if (encode.matches(currentPassword, user.getPassword()) && newPassword.equals(currentPassword) == true) {
				model.addAttribute("error", "Mật khẩu mới không được trùng với mật khẩu hiện tại!");
				return "/admin/profile/updatePassword";
			}
			else if (encode.matches(currentPassword, user.getPassword())==false) {
				model.addAttribute("error", "Nhập mật hiên tại không đúng!");
				return "/admin/profile/updatePassword";
			}
			else {
				String password = Password.encrytePassword(newPassword);
				user.setPassword(password);
				service.updateUser(user);
				model.addAttribute("message", "Cập nhật mật khẩu thành công!");
				return "/admin/profile/updatePassword";
			}
		} catch (Exception e) {
			// TODO: handle exception
			return "/admin/profile/updatePassword";
		}

	}
}
