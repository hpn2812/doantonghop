package ute.udn.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import ute.udn.entities.TestResult;
@Repository
public interface TestResultRepository extends JpaRepository<TestResult, Long> {
	
	@Query(value="select * from test_result tr  join  users us on tr.id=us.id  join test_exam te on te.test_exam_id=tr.test_exam_id where us.id=?1 ",nativeQuery = true)
	public 	Page<TestResult> getListTestResultByUserId(long id,Pageable pageable);
	
	//lấy danh sách kết quả thi theo id của người dùng --> thành viên
	@Query(value="select * from test_result tr  join  users us on tr.id=us.id  join test_exam te on te.test_exam_id=tr.test_exam_id where us.id=?1 and te.name like '%' ?2 '%'",nativeQuery = true)
	public 	Page<TestResult> getListTestResultByUserId(long id,String nameOfTestExam,Pageable pageable);
	
	//lấy dòng mới nhất ==> update dữ liệu khi làm nghe sang bài đọc
	@Query(value="SELECT * FROM test_result tr join users us on us.id=tr.id  where us.id=?1 ORDER BY result_id desc limit 1",nativeQuery = true)
	public 	TestResult getTestResultByUserId(long id);
	
	
	//thống kê có bao nhiêu lượt thi trong 1 tuần theo từng giáo viên ==> thống kê giáo viên
	@Query(value = "SELECT count(*) FROM test_result tr join test_exam te on te.test_exam_id=tr.test_exam_id where  te.id=?1 and time_start_listening_test BETWEEN (NOW() - INTERVAL 7 DAY) AND NOW();",nativeQuery = true)
	public int countTestResultOfWeek(long id);
	
	
	//lấy danh sách kết quả thi theo bài thi của giáo viên ==> giáo viên
	@Query(value = "SELECT * FROM test_result tr join test_exam te on te.test_exam_id=tr.test_exam_id where te.id=?1 ",nativeQuery = true)
	public List<TestResult> getListTestResultOfTeacher(long id);
	
	@Query(value = "SELECT * FROM test_result tr join test_exam te on te.test_exam_id=tr.test_exam_id where te.id=?1",nativeQuery = true)
	public Page<TestResult> getPageTestResultOfTeacher(long id,Pageable pageable);
	
	@Query(value = "SELECT * FROM test_result tr join users us on tr.id=us.id join test_exam te on te.test_exam_id=tr.test_exam_id where te.id=?1 and us.username like '%' ?2 '%' ",nativeQuery = true)
	public Page<TestResult> getPageTestResultOfTeacher(long id,String username,Pageable pageable);
	

}
