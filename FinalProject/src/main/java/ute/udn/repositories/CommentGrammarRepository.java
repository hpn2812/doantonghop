package ute.udn.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ute.udn.entities.CommentGrammar;

@Repository
public interface CommentGrammarRepository extends JpaRepository<CommentGrammar, Long>{
	
	List<CommentGrammar> findByDisplayAndGrammar_GrammarId(int display,long id);
	
	public Page<CommentGrammar> findByContentContainingAndDisplayAndNewComment(String content,int display, int newComment,Pageable pageable);
	
	public Page<CommentGrammar> findByContentContainingAndNewComment(String content, int newComment,Pageable pageable);
	
	public Page<CommentGrammar> findByDisplay(int display,Pageable pageable);
	
	public Page<CommentGrammar> findByNewComment(int newComment,Pageable pageable);
	
	public Page<CommentGrammar> findByContentContaining(String content,Pageable pageable);
	
	public Page<CommentGrammar> findByDisplayAndNewComment(int display,int newComment,Pageable pageable);
	
	public Page<CommentGrammar> findByDisplayAndContentContaining(int display,String content,Pageable pageable);
	
	public Page<CommentGrammar> findByNewCommentAndContentContaining(int newComment,String content,Pageable pageable);
	
	@Query(value = "SELECT count(*) FROM commentgrammar where new_comment=?1",nativeQuery = true)
	int countNewComment(int newComment);
	
	@Query(value="SELECT count(*) FROM commentgrammar where grammar_id=?1 and display=?2",nativeQuery = true)
	int countRecordComment(long id,int display);
	
}
