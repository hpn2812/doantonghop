package ute.udn.repositories;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ute.udn.entities.Grammar;
import ute.udn.entities.ListeningExcercise;
import ute.udn.entities.Vocabulary;

@Repository
public interface VocabularyRepository extends JpaRepository<Vocabulary, Long> , CrudRepository<Vocabulary, Long> {
	
	
	public Page<Vocabulary> findByNameContaining(String name,Pageable pageable);
	
	Page<Vocabulary> findByNameContainingAndUsers_Id(String name,long id,Pageable pageable);
	
	public 	Page<Vocabulary> findByUsers_Id(long id,Pageable pageable);
	
	@Query(value = "select count(*) from vocabulary where id=?1",nativeQuery = true)
	int countVocabularyByUserId(long id);
	
}
