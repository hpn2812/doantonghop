package ute.udn.repositories;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ute.udn.entities.Role;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long>, JpaRepository<Role, Long> {
	Role findByName(String name);
	
	@Query(value = "select * from roles r where r.id = ?1", nativeQuery = true)
	public Set<Role> findListByRoleId(List<Long> listRoleId);
}
