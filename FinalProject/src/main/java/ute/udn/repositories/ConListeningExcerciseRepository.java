package ute.udn.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ute.udn.entities.ContentListeningExcercise;

@Repository
public interface ConListeningExcerciseRepository extends JpaRepository<ContentListeningExcercise, Long>{

	@Query(value="select * from content_listening_excercise cle join listening l on cle.listen_id= l.listen_id where l.listen_id=?1",nativeQuery = true)
	public 	List<ContentListeningExcercise> getListListenQuestionByListenId(long listen_id);
	
	
	@Query(value="select * from content_listening_excercise cle join listening l on cle.listen_id= l.listen_id where l.listen_id=?1",nativeQuery = true)
	public 	Page<ContentListeningExcercise> getPageListenQuestionByListenId(Pageable pageable,long listen_id);

	ContentListeningExcercise findByAudio(String audio);

}
