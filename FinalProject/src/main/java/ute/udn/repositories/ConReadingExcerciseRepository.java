package ute.udn.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ute.udn.entities.ContentReadingExcercise;


@Repository
public interface ConReadingExcerciseRepository extends JpaRepository<ContentReadingExcercise, Long>{
	
	@Query(value="select * from content_reading_excercise cre join reading r on cre.read_id=r.read_id where  r.read_id=?1",nativeQuery = true)
	public 	List<ContentReadingExcercise> getListReadingQuestionByReadingId(long read_id);
	
	@Query(value="select * from content_reading_excercise cre join reading r on cre.read_id=r.read_id where  r.read_id=?1",nativeQuery = true)
	public 	Page<ContentReadingExcercise> getPageReadingQuestionByReadingId(Pageable pageable,long read_id);

}
