package ute.udn.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ute.udn.entities.Grammar;
import ute.udn.entities.TestExam;

@Repository
public interface TestExamRepository  extends JpaRepository<TestExam, Long>{
	
	Page<TestExam> findByNameContaining(String name,Pageable pageable);
	
	Page<TestExam> findByNameContainingAndUsers_Id(String name,long id,Pageable pageable);
	
	Page<TestExam> findByUsers_Id(long id,Pageable pageable);
	
	@Query(value = "select count(*) from test_exam where id=?1",nativeQuery = true)
	int countTestExamByUserId(long id);

}
