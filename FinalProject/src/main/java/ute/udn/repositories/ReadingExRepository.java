package ute.udn.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ute.udn.entities.ListeningExcercise;
import ute.udn.entities.ReadingExcercise;

@Repository
public interface ReadingExRepository extends JpaRepository<ReadingExcercise, Long> {
	
	String findByName(long id);
	
	public Page<ReadingExcercise> findByLevelAndPart(int part,int level,Pageable pageable);
	
	public Page<ReadingExcercise> findByPart(int part,Pageable pageable);
	
	public Page<ReadingExcercise> findByLevel(int level,Pageable pageable);

	public Page<ReadingExcercise> findByUsers_Id(long id,Pageable pageable);
	
	public Page<ReadingExcercise> findByLevelAndPartAndUsers_Id(int part,int level,Long id,Pageable pageable);
	
	public Page<ReadingExcercise> findByPartAndUsers_Id(int part,Long id,Pageable pageable);
	
	public Page<ReadingExcercise> findByLevelAndUsers_Id(int level,Long id,Pageable pageable);
	
	@Query(value = "select count(*) from reading where id=?1",nativeQuery = true)
	int countReadingExcerciseByUserId(long id);
}
