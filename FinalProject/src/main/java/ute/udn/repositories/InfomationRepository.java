package ute.udn.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ute.udn.entities.Infomation;
@Repository
public interface InfomationRepository  extends JpaRepository<Infomation, Long>{

}
