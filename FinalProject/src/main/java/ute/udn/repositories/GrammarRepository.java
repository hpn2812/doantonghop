package ute.udn.repositories;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ute.udn.entities.Grammar;

@Repository
public interface GrammarRepository  extends JpaRepository<Grammar, Long>, CrudRepository<Grammar, Long>{
	
	Page<Grammar> findByNameContaining(String name,Pageable pageable);
	
	Grammar findByNameLike(long id);

	Page<Grammar> findByNameContainingAndUsers_Id(String name,long id,Pageable pageable);
	
	public 	Page<Grammar> findByUsers_Id(long id,Pageable pageable);
	
	@Query(value = "select count(*) from grammars where id=?1",nativeQuery = true)
	int countByGrammarId(long id);
	
	
}
