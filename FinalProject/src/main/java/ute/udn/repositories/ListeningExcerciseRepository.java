package ute.udn.repositories;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ute.udn.entities.ListeningExcercise;

@Repository
public interface ListeningExcerciseRepository extends JpaRepository<ListeningExcercise, Long>,CrudRepository<ListeningExcercise, Long>{
	
	ListeningExcercise findByImage(String image);
	
	public Page<ListeningExcercise> findByLevelAndPart(int part,int level,Pageable pageable);
	
	public Page<ListeningExcercise> findByPart(int part,Pageable pageable);
	
	public Page<ListeningExcercise> findByLevel(int level,Pageable pageable);
	
	public Page<ListeningExcercise> findByUsers_Id(long id,Pageable pageable);
	
	public Page<ListeningExcercise> findByLevelAndPartAndUsers_Id(int part,int level,Long id,Pageable pageable);
	
	public Page<ListeningExcercise> findByPartAndUsers_Id(int part,Long id,Pageable pageable);
	
	public Page<ListeningExcercise> findByLevelAndUsers_Id(int level,Long id,Pageable pageable);
	
	@Query(value = "select count(*) from listening where id=?1",nativeQuery = true)
	int countListenExcerciseByUserId(long id);
	
}
