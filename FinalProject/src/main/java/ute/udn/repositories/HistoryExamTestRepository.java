package ute.udn.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ute.udn.entities.HistoryExamTest;


@Repository
public interface HistoryExamTestRepository extends JpaRepository<HistoryExamTest, Long> {
	
	
	@Query(value="select * from history_exam he join test_result tr on tr.result_id=he.result_id join test_exam_question teq on teq.question_id=he.question_id where tr.result_id=?1",nativeQuery = true)
	public 	List<HistoryExamTest> getListHistoryByResultId(long id);
	
	@Query(value="select * from history_exam he join test_result tr on tr.result_id=he.result_id join test_exam_question teq on teq.question_id=he.question_id where tr.result_id=?1",nativeQuery = true)
	public 	Page<HistoryExamTest> getPageHistoryByResultId(long id,Pageable pageable);
	
	

}
