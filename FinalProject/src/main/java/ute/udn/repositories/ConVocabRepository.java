package ute.udn.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import ute.udn.entities.ContentVocabulary;

@Repository
public interface ConVocabRepository extends JpaRepository<ContentVocabulary, Long> {
	@Query(value="select * from vocabulary vb join content_vocabulary cvb on vb.vocab_id= cvb.vocab_id where vb.vocab_id=?1",nativeQuery = true)
	public 	List<ContentVocabulary> getListContenVocabularyByVocabId(long vocab_id);
	
	@Query(value="select * from vocabulary vb join content_vocabulary cvb on vb.vocab_id= cvb.vocab_id where vb.vocab_id=?1",nativeQuery = true)
	public 	Page<ContentVocabulary> getPageableContenVocabularyByVocabId(long vocab_id,Pageable pageable);

}
