package ute.udn.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ute.udn.entities.ContentListeningExcercise;
import ute.udn.entities.TestExamQuestion;
@Repository
public interface TestExamQuestionRepository extends JpaRepository<TestExamQuestion, Long>{
	
	@Query(value="select * from test_exam_question teq join test_exam te on te.test_exam_id=teq.test_exam_id where te.test_exam_id=?1",nativeQuery = true)
	public 	List<TestExamQuestion> getListTestExamQuestionById(long id);
	
	
	@Query(value="select * from test_exam_question teq join test_exam te on te.test_exam_id=teq.test_exam_id where te.test_exam_id=?1",nativeQuery = true)
	public 	Page<TestExamQuestion> getPageTestExamQuestionById(long id,Pageable pageable);

}
