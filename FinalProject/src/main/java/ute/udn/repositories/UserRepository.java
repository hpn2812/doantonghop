package ute.udn.repositories;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ute.udn.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	//get list role 
	@Query(value = "select r.name from roles r join users_roles ur on ur.role_id=r.id join users u on u.id=ur.user_id where u.id=?1", nativeQuery = true)
    public List<String> findListRolesOfUser(long userId);
	
	//get user of role --> lọc theo role
	@Query(value = "select * from users u join users_roles ur on u.id=ur.user_id join roles r on ur.role_id=r.id where r.id=?1",nativeQuery = true)
	public List<User> search(long role);
	
	@Query(value = "select * from users u join users_roles ur on u.id=ur.user_id join roles r on ur.role_id=r.id where r.id=?1",nativeQuery = true)
	public Page<User> searchAndPageable(long role,Pageable pageable);
	
	@Query(value = "select count(*) from  users us join users_roles ur on us.id=ur.user_id where ur.role_id=?1",nativeQuery = true)
	int countUser(long role);
	
	User findByUsernameLike(String userName);

	User findByPhoneNumber(String phoneNumber);
	
	 
	
	
	
}
