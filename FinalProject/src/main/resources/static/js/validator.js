
function Validator(options){

    var formElement=document.querySelector(options.form);
    if(formElement){
        options.rules.forEach(function (rule) {
            var inputElement=formElement.querySelector(rule.selector);
         
            if(inputElement){
                inputElement.onblur=function(){
                
                
                    var errorMessage=rule.test(inputElement.value);
                    var errorElement=inputElement.parentElement.querySelector('.form-message'); 
                    
                    
                    
                    if(errorMessage){
                        errorElement.innerHTML= errorMessage;
                        inputElement.parentElement.classList.add('invalid');
                    }else{
                        errorElement.InnerText='';
                        inputElement.parentElement.classList.remove('invalid');
                    }
                }
            }

        });
        

    }
}

Validator.isRequired= function(selector){
    return {
        selector:selector,
        test:function(value){
            return value.trim() ? undefined: 'Tài khoản không được bỏ trống!';
        }
    };
}

Validator.isPassword= function(selector){
    return {
        selector:selector,
        test:function(value){
            return value.trim() ? undefined: 'Mật khẩu không được bỏ trống!';
        }
    };
}


